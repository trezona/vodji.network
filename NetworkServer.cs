﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Vodji.Network.Components;
using Vodji.Network.Components.Interfaces;
using Vodji.Network.Provider;
using Vodji.Shared.Background;
using Vodji.Shared.Utils;

namespace Vodji.Network
{
    public class NetworkServer : JobBackground
    {
        public NetworkMessageHandler<INetworkIdentity> NetworkMessageHandler { get; set; }
        public bool DisconnectInactiveConnections { get; set; }
        public INetworkProvider NetworkProvider { get; }

        public NetworkServer(INetworkProvider networkProvider)
        {
            NetworkProvider = networkProvider;
            {
                NetworkProvider.EventClientConnected.AddListener(OnClientConnected);
                NetworkProvider.EventClientDisconnected.AddListener(OnClientDisconnected);
                NetworkProvider.EventClientDataReceived.AddListener(OnClientDataReceived);
            }
        }

        public void Init()
        {
            _connections.Clear();
        }

        public bool SendMessage<T>(INetworkPoint networkPoint, T msg) where T : INetworkMessage
        {
            if(_connections.TryGetValue(networkPoint, out var networkIdentity))
            {
                networkIdentity.SendMessage(msg);
                return true;
            }

            return false;
        }

        public void SendMessage<T>(T msg) where T : INetworkMessage
        {
            foreach (var networkIdentity in _connections.KeysT2)
            {
                networkIdentity.SendMessage(msg);
            }
        }

        private void OnClientDataReceived(INetworkPoint networkPoint, byte[] bytesRecived)
        {
            if (_connections.TryGetValue(networkPoint, out var networkIdentity))
            {
                networkIdentity.OnDataReceived(bytesRecived);
                return;
            }

            Console.WriteLine($"HandleData Unknown connection {networkPoint}");
        }

        private void OnClientDisconnected(INetworkPoint networkPoint)
        {
            if(_connections.TryGetValue(networkPoint, out var networkIdentity))
            {
                networkIdentity.Disconnect();
                _connections.Remove(networkPoint);
            }
        }

        private void OnClientConnected(INetworkPoint networkPoint)
        {
            if(networkPoint.Id.DefaultGuid == default)
            {
                Console.WriteLine($"invalid connectionId: {networkPoint.Id}");
                return;
            }

            if(_connections.ContainsKey(networkPoint))
            {
                NetworkProvider.Disconnect(networkPoint);
                Console.WriteLine($"Connection {networkPoint} already in use.");
                return;
            }

            if(_connections.Count > NetworkProvider.MaxConnections)
            {
                NetworkProvider.Disconnect(networkPoint);
                Console.WriteLine($"Server {_connections.Count}/{NetworkProvider.MaxConnections}, Kicked client: {networkPoint}");
                return;
            }

            var networkIdentity = new NetworkIdentity(networkPoint);
            if(_connections.TryAdd(networkPoint, networkIdentity))
            {
                networkIdentity.NetworkMessageHandler = NetworkMessageHandler;
                return;
            }

            NetworkProvider.Disconnect(networkPoint);
            Console.WriteLine($"Server try register client, Kicked client: {networkPoint}");
        }

        public override void Dispose()
        {
            DisconnectClients();
            NetworkMessageHandler.Clear();

            NetworkProvider.EventClientConnected.RemoveListener(OnClientConnected);
            NetworkProvider.EventClientDisconnected.RemoveListener(OnClientDisconnected);
            NetworkProvider.EventClientDataReceived.RemoveListener(OnClientDataReceived);

            // NetworkProvider.Stop();
            base.Dispose();
        }

        public void DisconnectClients()
        {
            foreach(var networkIdentity in _connections.KeysT2)
            {
                networkIdentity.Disconnect();
            }
            _connections.Clear();
        }

        public override Task OnUpdateAsync(CancellationToken cancellationToken)
        {
            foreach (var networkIdentity in _connections.KeysT2)
            {
                if(DisconnectInactiveConnections && networkIdentity.IsTimeout(NetworkProvider.Timeout))
                {
                    Console.WriteLine($"Disconnecting {networkIdentity} for inactivity!");
                    networkIdentity.Disconnect();
                }

                //networkIdentity.Update();
            }
            return base.OnUpdateAsync(cancellationToken);
        }

        private DictionaryPair<INetworkPoint, INetworkIdentity> _connections = new();
    }
}
