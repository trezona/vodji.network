﻿using System;
using System.Collections.Generic;
using Vodji.Network.Components.Interfaces;
using Vodji.Shared.Extensions;

namespace Vodji.Network.Components
{
    public class NetworkMessageHandler<TClient> where TClient : class
    {
        public delegate void NetworkMessageHandlerDelegate(INetworkStream networkStream, TClient networkIdentity);

        public bool AddListener<T>(Action<T> function) where T : INetworkMessage
            => AddListener((T value, TClient _) => function(value));
        public bool AddListener<T>(Action<T, TClient> function) where T : INetworkMessage
        {
            var messageId = typeof(T).GetStableShortCode();
            var generatedDelegate = GetHandlerDelegate(function);
            if (messageHandlers.TryAdd(messageId, generatedDelegate))
            {
                Console.WriteLine($"Adding handler for {typeof(T).FullName}, id={messageId}.");
                return true;
            }

            Console.WriteLine($"If replacement is intentional, use ReplaceListener<{typeof(T).FullName}>.");
            return false;
        }

        public bool ReplaceListener<T>(Action<T> function) where T : INetworkMessage
            => ReplaceListener((T value, TClient _) => function(value));
        public bool ReplaceListener<T>(Action<T, TClient> function) where T : INetworkMessage
        {
            int messageId = typeof(T).GetStableShortCode();
            if (messageHandlers.TryGetValue(messageId, out _))
            {
                messageHandlers[messageId] = GetHandlerDelegate(function);
                Console.WriteLine($"Replacing handler {typeof(T).FullName}, id={messageId}.");
                return true;
            }

            //Обработчик отсуствует, необходимо добавить новый AddListener
            Console.WriteLine($"The handler is missing, you need to add a new AddListener<{typeof(T).FullName}>");
            return false;
        }

        public bool RemoveListener(int messageId)
        {
            if (messageHandlers.Remove(messageId))
            {
                Console.WriteLine($"The {messageId} handler removed.");
                return true;
            }

            Console.WriteLine($"The {messageId} handler is missing, dont remove.");
            return false;
        }
        public bool RemoveListener<T>() where T : INetworkMessage
        {
            int messageId = typeof(T).GetStableShortCode();
            if (messageHandlers.Remove(messageId))
            {
                Console.WriteLine($"The {typeof(T).FullName} handler removed.");
                return true;
            }

            Console.WriteLine($"The {typeof(T).FullName} handler is missing, dont remove.");
            return false;
        }

        public bool Invoke(INetworkStream networkStream, TClient networkIdentity)
        {
            if (Unpack(networkStream, out var messageId))
            {
                Console.WriteLine($"msgType:{messageId} Received: {networkStream.Length} via from {networkIdentity}");
                if (messageHandlers.TryGetValue(messageId, out var function))
                {
                    function.Invoke(networkStream, networkIdentity);
                    return true;
                }

                Console.WriteLine($"Unknown message ID {messageId}.");
                return false;
            }

            Console.WriteLine($"Closed connection: {networkIdentity}. Invalid message header.");
            return false;
        }
        protected bool Unpack(INetworkStream networkStream, out ushort messageId)
        {
            messageId = default;
            try
            {
                networkStream.Read(out messageId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return !messageId.Equals(default);
        }

        public void Clear() => messageHandlers.Clear();

        protected NetworkMessageHandlerDelegate GetHandlerDelegate<T>(Action<T, TClient> function) where T : INetworkMessage
        {
            return (networkStream, networkIdentity) =>
            {
                try
                {
                    var networkMessage = Activator.CreateInstance<T>();
                    networkMessage.Deserialize(networkStream);
                    function.Invoke(networkMessage, networkIdentity);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Closed connection: {networkIdentity}. Invalid data. Reason: {e}");
                }
            };
        }
        protected Dictionary<int, NetworkMessageHandlerDelegate> messageHandlers = new();
    }
}
