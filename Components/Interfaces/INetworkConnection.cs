﻿namespace Vodji.Network.Components.Interfaces
{
    public interface INetworkConnection
    {
        public void SendMessage<T>(T msg, int channel) where T : INetworkMessage;
        public void OnReceivedData(byte[] bytesRecived);
        public void Disconnect();
    }
}
