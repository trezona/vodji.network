﻿namespace Vodji.Network.Components.Interfaces
{
    public interface INetworkMessage
    {
        public void Serialize(INetworkStream networkStream);
        public void Deserialize(INetworkStream networkStream);
    }
}
