﻿using System;

namespace Vodji.Network.Components.Interfaces
{
    public interface INetworkStream
    {
        public int Length { get; }
        public int Position { get; set; }

        public void Write(object value);
        public object Read<T>() => Read(typeof(T));
        public object Read(Type valueType);

        // Bool | SByte | Byte = 1
        public void Write(bool value) => Write((byte)(value ? 1 : 0));
        public void Write(sbyte value) => Write((byte)value);
        public void Write(byte value);

        public bool Read(out bool value) => value = Read(out byte _) == 1 ? true : false;
        public sbyte Read(out sbyte value) => value = (sbyte)Read(out byte _);
        public byte Read(out byte value);

        // Char | Short | UShort = 2
        public void Write(char value) => Write((ushort)value);
        public void Write(short value) => Write((ushort)value);
        public void Write(ushort value);
  
        public char Read(out char value) => value = (char)Read(out ushort _);
        public short Read(out short value) => value = (short)Read(out ushort _);
        public ushort Read(out ushort value);

        // Int | UInt = 4
        public void Write(int value) => Write((uint)value);
        public void Write(uint value);

        public int Read(out int value) => value = (int)Read(out uint _);
        public uint Read(out uint value);

        // Float = 4
        public void Write(float value);
        public float Read(out float value);

        // DateTime = 8
        public void Write(DateTime value);
        public DateTime Read(out DateTime value);

        // Double = 8
        public void Write(double value);
        public double Read(out double value);

        // Long = 8
        public void Write(long value) => Write((ulong)value);
        public void Write(ulong value);

        public long Read(out long value) => value = (long)Read(out ulong _);
        public ulong Read(out ulong value);

        // Decimal = 16
        public void Write(decimal value);
        public decimal Read(out decimal value);

        // String = ?
        public string Read(out string value);

        // Bytes = ?
        public void Write(byte[] value);
        public void Write(byte[] value, int length);

        public byte[] Read(out byte[] value);
        public byte[] Read(out byte[] value, int length);

        public byte[] ToArray();
    }
}
