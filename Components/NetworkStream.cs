﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Vodji.Network.Components.Interfaces;

namespace Vodji.Network.Components
{
    public class NetworkStream : INetworkStream
    {
        public NetworkStream() : this(new byte[0]) { }
        public NetworkStream(byte[] bytes)
        {
            _dataBuffer = new byte[0];
            _UTF8Encoding = new();
        }

        public int Length { get; protected set; }
        public int Position { get; set; }

        public void Write(object value)
        {
            // Bool | SByte | Byte = 1
            if (value is bool valueBool) Write(valueBool);
            else if (value is sbyte valueSbyte) Write(valueSbyte);
            else if (value is byte valueByte) Write(valueByte);

            // Char | Short | UShort = 2
            else if (value is char valueChar) Write(valueChar);
            else if (value is short valueShort) Write(valueShort);
            else if (value is ushort valueUShort) Write(valueUShort);

            // Int | UInt | Float = 4
            else if (value is int valueInt) Write(valueInt);
            else if (value is uint valueUInt) Write(valueUInt);
            else if (value is float valueFloat) Write(valueFloat);

            // DateTime | Double | Long | ULong  = 8
            else if (value is DateTime valueDateTime) Write(valueDateTime);
            else if (value is double valueDobule) Write(valueDobule);
            else if (value is long valueLong) Write(valueLong);
            else if (value is ulong valueULong) Write(valueULong);

            // Decimal = 16
            else if (value is decimal valueDecimal) Write(valueDecimal);

            // Byte | String = ??
            else if (value is byte[] valueBytes) Write(valueBytes);
            else if (value is string valueString) Write(valueString);
        }
        public virtual object Read(Type valueType)
        {
            // Bool | SByte | Byte = 1
            if (valueType.Equals(typeof(bool))) return Read(out bool value);
            if (valueType.Equals(typeof(sbyte))) return Read(out sbyte value);
            if (valueType.Equals(typeof(byte))) return Read(out byte _);

            // Char | Short | UShort = 2
            if (valueType.Equals(typeof(char))) return Read(out char _);
            if (valueType.Equals(typeof(short))) return Read(out bool _);
            if (valueType.Equals(typeof(ushort))) return Read(out bool _);

            // Int | UInt | Float = 4
            if (valueType.Equals(typeof(int))) return Read(out int _);
            if (valueType.Equals(typeof(uint))) return Read(out uint _);
            if (valueType.Equals(typeof(float))) return Read(out float _);

            // DateTime | Double | Long | ULong  = 8
            if (valueType.Equals(typeof(DateTime))) return Read(out DateTime _);
            if (valueType.Equals(typeof(double))) return Read(out double _);
            if (valueType.Equals(typeof(long))) return Read(out long _);
            if (valueType.Equals(typeof(ulong))) return Read(out ulong _);

            // Decimal = 16
            if (valueType.Equals(typeof(decimal))) return Read(out bool _);

            // Byte | String = ??
            if (valueType.Equals(typeof(string))) return Read(out bool _);
            if (valueType.Equals(typeof(byte[]))) return Read(out bool _);

            return default;
        }

        // Bool | SByte | Byte = 1
        public void Write(bool value) => Write((byte)(value ? 1 : 0));
        public void Write(sbyte value) => Write((byte)value);
        public void Write(byte value)
        {
            Ensure(1);
            unchecked
            {
                _dataBuffer[Position++] = value;
            }
        }
        public bool Read(out bool value) => value = Read(out byte _) == 1 ? true : false;
        public sbyte Read(out sbyte value) => value = (sbyte)Read(out byte _);
        public byte Read(out byte value)
        {
            value = default;
            try { value = _dataBuffer[Position++]; }
            catch (Exception e) { Console.WriteLine(e); }
            return value;
        }

        // Char | Short | UShort = 2
        public void Write(char value) => Write((ushort)value);
        public void Write(short value) => Write((ushort)value);
        public void Write(ushort value)
        {
            Write((byte)value);
            Write((byte)(value >> 8));
        }
        public char Read(out char value) => value = (char)Read(out ushort _);
        public short Read(out short value) => value = (short)Read(out ushort _);
        public ushort Read(out ushort value)
        {
            value = default;
            value |= Read(out byte _);
            value |= (ushort)(Read(out byte _) << 8);
            return value;
        }

        // Int | UInt = 4
        public void Write(int value) => Write((uint)value);
        public unsafe void Write(uint value)
        {
            unchecked
            {
                Ensure(4);
                _dataBuffer[Position++] = (byte)value;
                _dataBuffer[Position++] = (byte)(value >> 8);
                _dataBuffer[Position++] = (byte)(value >> 16);
                _dataBuffer[Position++] = (byte)(value >> 24);
            }
        }
        public int Read(out int value) => value = (int)Read(out uint _);
        public uint Read(out uint value)
        {
            value = default;
            value |= Read(out byte _);
            value |= (uint)(Read(out byte _) << 8);
            value |= (uint)(Read(out byte _) << 16);
            value |= (uint)(Read(out byte _) << 24);
            return value;
        }

        // Float = 4
        public unsafe void Write(float value)
        {
            var buffer = new byte[4];
            fixed (byte* bufferRef = buffer)
            {
                *((float*)bufferRef) = value;
            }
            Write(buffer, buffer.Length);
        }
        public float Read(out float value)
        {
            Read(out byte[] bytes, 4);
            return value = MemoryMarshal.Cast<byte, float>(bytes)[0];
        }

        // DateTime = 8
        public unsafe void Write(DateTime value)
        {
            unchecked
            {
                var buffer = new byte[8];
                fixed (byte* bufferRef = buffer)
                {
                    *((DateTime*)bufferRef) = value;
                }
                Write(buffer, buffer.Length);
            }
        }
        public DateTime Read(out DateTime value)
        {
            Read(out byte[] bytes, 8);
            return value = MemoryMarshal.Cast<byte, DateTime>(bytes)[0];
        }

        // Double = 8
        public unsafe void Write(double value)
        {
            unchecked
            {
                var buffer = new byte[8];
                fixed (byte* bufferRef = buffer)
                {
                    *((double*)bufferRef) = value;
                }
                Write(buffer, buffer.Length);
            }

        }
        public double Read(out double value)
        {
            Read(out byte[] bytes, 8);
            return value = MemoryMarshal.Cast<byte, double>(bytes)[0];
        }

        // Long = 8
        public void Write(long value) => Write((ulong)value);
        public unsafe void Write(ulong value)
        {
            unchecked
            {
                var buffer = new byte[8];
                fixed (byte* bufferRef = buffer)
                {
                    *((ulong*)bufferRef) = value;
                }
                Write(buffer, buffer.Length);
            }
        }
        public long Read(out long value) => value = (long)Read(out ulong _);
        public ulong Read(out ulong value)
        {
            Read(out byte[] bytes, 8);
            return value = MemoryMarshal.Cast<byte, ulong>(bytes)[0];
        }

        // Decimal = 16
        public unsafe void Write(decimal value)
        {
            unchecked
            {
                var buffer = new byte[16];
                fixed (byte* bufferRef = buffer)
                {
                    *((decimal*)bufferRef) = value;
                }
                Write(buffer, buffer.Length);
            }
        }
        public decimal Read(out decimal value)
        {
            Read(out byte[] bytes, 16);
            return value = MemoryMarshal.Cast<byte, decimal>(bytes)[0];
        }

        // String = ?
        public void Write(string value)
        {
            var bytes = _UTF8Encoding.GetBytes(value, 0, value.Length);
            Write(bytes);
        }
        public string Read(out string value)
        {
            Read(out byte[] bytes);
            return value = _UTF8Encoding.GetString(bytes);
        }

        // Bytes = ?
        public void Write(byte[] value)
        {
            Write((ushort)value.Length);
            Write(value, value.Length);
        }
        public void Write(byte[] value, int length)
        {
            Ensure(length);
            unchecked
            {
                Array.ConstrainedCopy(value, 0, _dataBuffer, Position, length);
                Position = Length;
            }
        }

        public byte[] Read(out byte[] value)
        {
            Read(out ushort length);
            return Read(out value, (int)length);
        }
        public byte[] Read(out byte[] value, int length)
        {
            value = new byte[length];
            if (length > 0)
            {
                Array.ConstrainedCopy(_dataBuffer, Position, value, 0, length);
                Position += length;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Ensure(int length)
        {
            if (length > 0)
            {
                Length = _dataBuffer.Length + length;
                Array.Resize(ref _dataBuffer, Length);
            }
        }

        public byte[] ToArray() => _dataBuffer;

        public void Clear()
        {
            _dataBuffer = new byte[0];
            Position = Length = 0;
        }

        private byte[] _dataBuffer;
        private readonly UTF8Encoding _UTF8Encoding;
    }
}
