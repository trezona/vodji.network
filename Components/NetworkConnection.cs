﻿using Vodji.Network.Components.Interfaces;
using Vodji.Network.Provider.Interfaces;
using Vodji.Shared.Extensions;

namespace Vodji.Network.Components
{
    public class NetworkConnection : INetworkConnection
    {
        public NetworkMessageHandler<INetworkConnection> NetworkMessageHandler { get; set; }

        public NetworkConnection(INetworkBaseClient networkClient)
        {
            _networkClient = networkClient;
        }

        public void Disconnect()
        {
            _networkClient.Disconnect();
        }

        public void OnReceivedData(byte[] bytesRecived)
        {
            var networkStream = new NetworkStream(bytesRecived);
            NetworkMessageHandler.Invoke(networkStream, this);
        }

        public void SendMessage<T>(T msg, int channel) where T : INetworkMessage
        {
            var networkStream = new NetworkStream();
            networkStream.Write(typeof(T).GetStableShortCode());
            msg.Serialize(networkStream);
            _networkClient.Send(networkStream.ToArray(), channel);
        }

        public override string ToString() => _networkClient.ToString();
        private INetworkBaseClient _networkClient;
    }
}
