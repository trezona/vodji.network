﻿using System;

namespace Vodji.Network.Components
{
    public struct NetworkId
    {
        public Guid DefaultGuid { get; set; }

        public NetworkId(Guid guid) => DefaultGuid = guid;
        public NetworkId(string id)
        {
            DefaultGuid = default;
            if (string.IsNullOrWhiteSpace(id)) return;

            if(Guid.TryParse(id, out var value))
            {
                DefaultGuid = value;
                return;
            }

            id = id.Replace("_", "/").Replace("-", "+");
            byte[] buffer = Convert.FromBase64String(id + "==");
            DefaultGuid = new Guid(buffer);
        }

        public override string ToString()
        {
            var base64 = Convert.ToBase64String(DefaultGuid.ToByteArray());
            base64 = base64.Replace("/", "_").Replace("+", "-");
            return base64.Substring(0, 22);
        }

        public static NetworkId NewGuid() => new NetworkId(Guid.NewGuid());
        public override int GetHashCode() => DefaultGuid.GetHashCode();
        public override bool Equals(object obj)
        {
            return obj switch
            {
                NetworkId networkId => DefaultGuid.Equals(networkId.DefaultGuid),
                Guid defaultGuid => DefaultGuid.Equals(defaultGuid),
                string stringId => ToString().Equals(stringId),
                _ => false
            };
        }
    }
}
