﻿using System;
using System.Collections.Generic;
using Vodji.Network.Components.Interfaces;

namespace Vodji.Network.Components
{
    public class NetworkIdentity : INetworkIdentity
    {
        public INetworkPoint NetworkPoint { get; }
        public NetworkMessageHandler<INetworkIdentity> NetworkMessageHandler { get; set; }

        public NetworkIdentity(INetworkPoint networkPoint)
        {
            NetworkPoint = networkPoint;
        }

        public bool IsTimeout(float timeout)
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            while(_queueNetworkMessages.TryDequeue(out var networkMessage))
            {

            }
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public void SendMessage<T>(T msg)
        {
            throw new NotImplementedException();
        }

        public void OnDataReceived(byte[] bytesRecived)
        {
            throw new NotImplementedException();
        }

        private Queue<INetworkMessage> _queueNetworkMessages;
    }
}
