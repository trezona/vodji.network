﻿//using System;
//using System.Net;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Core.Interfaces;
//using Vodji.Network.Identity;
//using Vodji.Network.Identity.Interfaces;
//using Vodji.Shared.Utils;

//namespace Vodji.Network.Provider.Base
//{
//    public class NetworkSocketProvider : NetworkSocket, INetworkProvider
//    {
//        protected override void OnRecivedData(IPEndPoint ipEndPoint, byte[] recivedBytes)
//        {
//            INetworkPoint networkPoint;
//            if(!connections.TryGetValue(ipEndPoint, out networkPoint))
//            {
//                networkPoint = new NetworkPoint(this);
//                connections.Add(ipEndPoint, networkPoint);
//                LoggingManager.GetLogger().LogInformation($"New connection: {networkPoint}");
//                EventClientConnected.Invoke(networkPoint);
//            }

//            EventDataReceived.Invoke(networkPoint, recivedBytes);
//        }
//        protected override void OnSendData(int sentNumBytes, IPEndPoint clientPoint) { }

//        public bool SendMessage(INetworkPoint networkPoint, byte[] bytes)
//        {
//            if (connections.TryGetValue(networkPoint, out var ipEndPoint))
//            {
//                LoggingManager.GetLogger().LogError($"Message sent: {networkPoint}");
//                return SendPacket(bytes, ipEndPoint) == System.Net.Sockets.SocketError.Success;
//            }

//            LoggingManager.GetLogger().LogError($"[FAIL] Message sent: {networkPoint}");
//            return false;
//        }

//        public void Connect(IPEndPoint server) => SendPacket(new byte[10], server);

//        public bool Kick(INetworkPoint networkPoint)
//        {
//            if (connections.Contains(networkPoint))
//            {
//                LoggingManager.GetLogger().LogInformation($"Remove connection: {networkPoint}");
//                connections.Remove(networkPoint);
//                return true;
//            }

//            LoggingManager.GetLogger().LogInformation($"[FAIL] Remove connection: {networkPoint}");
//            return false;
//        }

//        public void StartServer()
//        {
//            throw new NotImplementedException();
//        }

//        public EventCallback<INetworkPoint, byte[]> EventDataReceived { get; } = new EventCallback<INetworkPoint, byte[]>();
//        public EventCallback<INetworkPoint, int> EventDataSend { get; } = new EventCallback<INetworkPoint, int>();
//        public EventCallback<INetworkPoint> EventClientConnected { get; } = new EventCallback<INetworkPoint>();
//        public EventCallback<INetworkPoint> EventClientDisconnected { get; } = new EventCallback<INetworkPoint>();

//        public INetworkOptions NetworkOptions { get; }
//        public int MaxConnections { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

//        protected DictionaryPair<INetworkPoint, IPEndPoint> connections = new DictionaryPair<INetworkPoint, IPEndPoint>();
//    }
//}
