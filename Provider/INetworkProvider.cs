﻿using Vodji.Network.Components.Interfaces;
using Vodji.Shared.Utils;

namespace Vodji.Network.Provider
{
    public interface INetworkProvider
    {
        // Server
        public int MaxConnections { get; set; }
        public float Timeout { get; set; }

        public EventCallback<INetworkPoint> EventClientConnected { get; }
        public EventCallback<INetworkPoint> EventClientDisconnected { get; }
        public EventCallback<INetworkPoint, byte[]> EventClientDataReceived { get; }

        public void Disconnect(INetworkPoint networkPoint);

        // Client
        public EventCallback EventConnected { get; }
        public EventCallback EventDisconnected { get; }
        public EventCallback<byte[]> EventDataReceived { get; }





        public void StartServer();




        public bool SendMessage(INetworkPoint networkPoint, byte[] messageBytes);
        public void Listen();
    }
}
