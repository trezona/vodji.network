﻿using System.Threading.Tasks;
using Vodji.Shared.Utils;

namespace Vodji.Network.Provider.Interfaces
{
    public interface INetworkBaseClient
    {
        public EventCallback EventConnected { get; }
        public EventCallback EventDisconnected { get; }
        public EventCallback<byte[]> EventReceivedData { get; }

        public int ConnectTo(INetworkAddress networkAddress) => ConnectToAsync(networkAddress).Result;
        public Task<int> ConnectToAsync(INetworkAddress networkAddress);
        public void Send(byte[] bytesMessage, int channel);
        public void Disconnect();
    }
}
