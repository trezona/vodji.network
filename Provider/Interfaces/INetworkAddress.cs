﻿namespace Vodji.Network.Provider.Interfaces
{
    public interface INetworkAddress
    {
        public object Value { get; }
    }
}
