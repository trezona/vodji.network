﻿using Vodji.Network.Provider.Interfaces;
using Vodji.Steam.APISteamworks.Structs;

namespace Vodji.Network.Provider.P2PSocketSteam
{
    public struct P2PSocketSteamAddress : INetworkAddress
    {
        public object Value { get; }
        public SteamID SteamID { get; }
     
        /// <summary>
        /// For Server
        /// </summary>
        /// <param name="virtualPort"></param>
        public P2PSocketSteamAddress(int virtualPort)
        {
            SteamID = default;
            Value = virtualPort;
        }

        /// <summary>
        /// For Client
        /// </summary>
        /// <param name="steamID64">Connect SteamUser</param>
        /// <param name="virtualPort"></param>
        public P2PSocketSteamAddress(SteamID steamID, int virtualPort)
        {
            SteamID = steamID;
            Value = virtualPort;
        }
    }
}
