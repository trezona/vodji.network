﻿using Steamworks;
using Vodji.Network.Components;
using Vodji.Network.Components.Interfaces;

namespace Vodji.Network.Provider.P2PSocketSteam
{
    public class P2PSocketSteamPoint : INetworkPoint
    {
        public NetworkId Id { get; }
        public ulong SteamID64 { get; }
        public object Connection { get; }

        public P2PSocketSteamPoint(ulong steamID64, HSteamNetConnection connection)
        {
            Id = NetworkId.NewGuid();
            SteamID64 = steamID64;
            Connection = connection;
        }

        public override string ToString() => SteamID64.ToString();
    }
}
