﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Steamworks;
using Vodji.Network.Components.Interfaces;
using Vodji.Network.Provider.Interfaces;
using Vodji.Network.Provider.P2PSocketSteam;
using Vodji.Shared.Background;
using Vodji.Shared.Utils;
using Vodji.Steam.APISteamworks.Enums;
using Vodji.Steam.APISteamworks.Interfaces;
using Vodji.Steam.APISteamworks.Interfaces.Extensions;
using Vodji.Steam.APISteamworks.Structs;
using Vodji.Steam.APISteamworks.Structs.Extensions;

namespace Vodji.Network.Provider.SocketSteam
{
    public class P2PSocketSteamServer : JobBackground, IDisposable, INetworkBaseServer
    {
        public int MaxConnections { get; set; } = 50;
        public float Timeout { get; set; } = 25;

        public EventCallback<INetworkPoint> EventClientConnected { get; } = new();
        public EventCallback<INetworkPoint> EventClientDisconnected { get; } = new();
        public EventCallback<INetworkPoint, byte[]> EventClientDataReceived { get; } = new();

        public void Listen(INetworkAddress networkAddress)
        {
            if(networkAddress is P2PSocketSteamAddress)
            {
                _socketAddress = SteamID.GetSteamID();
                _socketVirtualPort = (int)networkAddress.Value;
                _socket = ISteamNetworkingSockets.Default.CreateListenSocketP2P((int)networkAddress.Value, 0, default);
                Console.WriteLine($"Steam {SteamID.GetSteamID()}({_socket.m_HSteamListenSocket}): {networkAddress.Value} Listen.");
            }

            Console.WriteLine($"Value networkAddress({networkAddress.GetType()}) is never type value. Needed {typeof(P2PSocketSteamAddress).Name}");
        }
        public bool Disconnect(INetworkPoint networkPoint)
        {
            if (_connections.ContainsKey(networkPoint))
            {

                Console.WriteLine($"Client {networkPoint} kicked.");
                SteamNetworkingSockets.CloseConnection((HSteamNetConnection)networkPoint.Connection, 0, "Kicked by server", false);
                EventClientDisconnected.Invoke(networkPoint);
                _connections.Remove(networkPoint);
                return true;
            }

            Console.WriteLine("Trying to disconnect unknown connection " + networkPoint);
            return false;
        }
        public void Send(INetworkPoint networkPoint, byte[] data, int channel)
        {
            if (_connections.ContainsKey(networkPoint))
            {
                var eResult = ISteamNetworkingSockets.Default.SendMessageToConnection((HSteamNetConnection)networkPoint.Connection, data, (SteamNetworkingSend)channel, out _);
                switch (eResult)
                {
                    case EResult.k_EResultNoConnection:
                    case EResult.k_EResultInvalidParam:
                        Console.WriteLine($"Connection to {networkPoint} was lost.");
                        Disconnect(networkPoint);
                        break;
                    case EResult.k_EResultOK:
                        Console.WriteLine($"Could not send to {networkPoint} -> {eResult}");
                        break;
                };
            }
            else
            {
                Console.WriteLine("Trying to send on unknown connection: " + networkPoint);
            }
        }

        private void OnStateChanged(SteamNetConnectionStatusChangedCallback_t arg1)
        {
            var socket = arg1.m_info.m_hListenSocket;
            if (socket == _socket) // Bug fixed
            {
                INetworkPoint networkPoint;
                var connection = arg1.m_hConn;
                var connectionUser = arg1.m_info.m_identityRemote.GetSteamID64();
                var connectionState = arg1.m_info.m_eState;

                switch (connectionState)
                {
                    case ESteamNetworkingConnectionState.k_ESteamNetworkingConnectionState_Connecting:
                        if (_connections.Count >= MaxConnections)
                        {
                            Console.WriteLine($"Incoming connection {connectionUser} would exceed max connection {MaxConnections}. Rejecting.");
                            SteamNetworkingSockets.CloseConnection(connection, 0, "Max Connection Count", false);
                            return;
                        }

                        var eResult = SteamNetworkingSockets.AcceptConnection(connection);
                        switch (eResult)
                        {
                            case EResult.k_EResultOK:
                                Console.WriteLine($"Client {connectionUser} Accepting connection.");
                                break;
                            default:
                                Console.WriteLine($"Client {connectionUser} could not be accepted: {eResult}");
                                break;
                        }
                        break;

                    case ESteamNetworkingConnectionState.k_ESteamNetworkingConnectionState_Connected:
                        networkPoint = new P2PSocketSteamPoint(connectionUser, connection);
                        if (!_connections.TryAdd(connectionUser, networkPoint))
                        {
                            Console.WriteLine($"Client {connectionUser} disconnected to many connection.");
                            SteamNetworkingSockets.CloseConnection(connection, 0, "To many Connection", false);
                        }

                        Console.WriteLine($"Client with {connectionUser}(#{_connections.Count}) connected.");
                        EventClientConnected.Invoke(networkPoint);
                        break;

                    case ESteamNetworkingConnectionState.k_ESteamNetworkingConnectionState_ClosedByPeer:
                        if (_connections.TryGetValue(connectionUser, out networkPoint))
                        {
                            Disconnect(networkPoint);
                            break;
                        }
                        Console.WriteLine($"Client {connectionUser} not exist: {connectionState}");
                        break;

                    default:
                        Console.WriteLine($"[SERVER] Client {connectionUser} state now: {connectionState}");
                        break;
                }
            }
        }
        private void OnReceiveData()
        {
            foreach (P2PSocketSteamPoint networkPoint in _connections.KeysT2)
            {
                SteamNetworkingSockets.FlushMessagesOnConnection((HSteamNetConnection)networkPoint.Connection);
                foreach (var steamNetworkingMessage in ISteamNetworkingSockets.Default.ReceiveMessagesOnConnection((HSteamNetConnection)networkPoint.Connection))
                {
                    EventClientDataReceived.Invoke(networkPoint, steamNetworkingMessage.GetData());
                }
            }
        }

        public override void Dispose()
        {
            SteamNetworkingSockets.CloseListenSocket(_socket);
            ISteamNetworkingSockets.Default.EventSteamNetConnectionStatusChangedCallback.RemoveListener(OnStateChanged);
            base.Dispose();
        }

        public P2PSocketSteamServer(bool relayNetwork = true)
        {
            if (relayNetwork) SteamNetworkingUtils.InitRelayNetworkAccess();
            ISteamNetworkingSockets.Default.EventSteamNetConnectionStatusChangedCallback.AddListener(OnStateChanged);
        }

        public override Task OnUpdateAsync(CancellationToken cancellationToken)
        {
            OnReceiveData();
            return base.OnUpdateAsync(cancellationToken);
        }

        public override string ToString() => $"{_socketAddress}({_socket}:{_socketVirtualPort})";

        private ulong _socketAddress;
        private int _socketVirtualPort;
        private HSteamListenSocket _socket;
        private ConcurrentDictionaryPair<ulong, INetworkPoint> _connections = new();
    }
}
