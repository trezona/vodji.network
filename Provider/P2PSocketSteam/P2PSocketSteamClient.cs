﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Steamworks;
using Vodji.Network.Provider.Interfaces;
using Vodji.Shared.Background;
using Vodji.Shared.Utils;
using Vodji.Steam.APISteamworks.Enums;
using Vodji.Steam.APISteamworks.Interfaces;
using Vodji.Steam.APISteamworks.Interfaces.Extensions;
using Vodji.Steam.APISteamworks.Structs;
using Vodji.Steam.APISteamworks.Structs.Extensions;

namespace Vodji.Network.Provider.P2PSocketSteam
{
    public enum SteamConnectionState
    {
        None,
        Connecting,
        Connected,
        Broken,
        Timeout,
        Disconnected,
        Cancelled
    }

    public class P2PSocketSteamClient : JobBackground, INetworkBaseClient, IDisposable
    {
        public SteamConnectionState ConnectionState { get; protected set; }
        public TimeSpan ConnectionTimeout { get; set; } = TimeSpan.FromSeconds(15);

        public EventCallback EventConnected { get; } = new();
        public EventCallback EventDisconnected { get; } = new();
        public EventCallback<byte[]> EventReceivedData { get; } = new();

        public void Send(byte[] data, int channel)
        {
            var eResult = ISteamNetworkingSockets.Default.SendMessageToConnection(_connection, data, (SteamNetworkingSend)channel, out _);
            if (eResult == EResult.k_EResultNoConnection || eResult == EResult.k_EResultInvalidParam)
            {
                Console.WriteLine($"Connection to {this} was lost.");
                Disconnect();
            }
            else if (eResult != EResult.k_EResultOK)
            {
                Console.WriteLine($"Could not send to {this} -> {eResult}");
            }
        }
        public void Disconnect()
        {
            if (_connection != default)
            {
                ISteamNetworkingSockets.Default.CloseConnection(_connection, 0, "Disconnect", false);
                ConnectionState = SteamConnectionState.Disconnected;
                Console.WriteLine($"{this} Disconnect");
                EventDisconnected.Invoke();
                _connection = default;
            }
        }
        public async Task<int> ConnectToAsync(INetworkAddress networkAddress)
        {
            if (networkAddress is P2PSocketSteamAddress steamAddress)
            {
                _connectionSteamID = steamAddress.SteamID;
                _connectionVirtualPort = (int)steamAddress.Value;
                try
                {
                    ConnectionState = SteamConnectionState.Connecting;
                    SteamNetworkingIdentity steamNetworkingIdentity = new();
                    {
                        steamNetworkingIdentity.SetSteamID(_connectionSteamID);
                    }

                    _connection = ISteamNetworkingSockets.Default.ConnectP2P(ref steamNetworkingIdentity, _connectionVirtualPort, 0, default);
                    var authTimeout = DateTime.UtcNow;
                    ConnectionState = await Task.Factory.StartNew(() =>
                    {
                        while (ConnectionState == SteamConnectionState.Connecting)
                        {
                            if (DateTime.UtcNow - authTimeout > ConnectionTimeout)
                            {
                                Console.WriteLine($"Connection to {this} Timeout {ConnectionTimeout}s.");
                                return SteamConnectionState.Timeout;
                            }
                        }

                        if (ConnectionState != SteamConnectionState.Connected)
                        {
                            Console.WriteLine($"The connection state was cancelled.");
                            return SteamConnectionState.Cancelled;
                        }

                        return ConnectionState;
                    });
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Connection string was not in the right format. Did you enter a SteamId?");
                    ConnectionState = SteamConnectionState.Broken;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    ConnectionState = SteamConnectionState.Broken;
                }
                finally
                {
                    if (ConnectionState == SteamConnectionState.Broken)
                    {
                        Console.WriteLine("Connection state is broken.");
                    }
                }
            }
            else
            {
                Console.WriteLine($"Value networkAddress is never type value. Needed {typeof(P2PSocketSteamAddress).Name}");
                ConnectionState = SteamConnectionState.Broken;
            }

            return (int)ConnectionState;
        }

        public override void Dispose()
        {
            ISteamNetworkingSockets.Default.EventSteamNetConnectionStatusChangedCallback.RemoveListener(OnConnectionStatusChanged);
            Disconnect();
            base.Dispose();
        }
        private void OnConnectionStatusChanged(SteamNetConnectionStatusChangedCallback_t param)
        {
            // Client => Server
            var connection = param.m_hConn;
            if (connection == _connection) // Bug Fixed
            {
                var connectionState = param.m_info.m_eState;
                switch (connectionState)
                {
                    case ESteamNetworkingConnectionState.k_ESteamNetworkingConnectionState_Connected:
                        Console.WriteLine($"Connected to {this}({_connection})");
                        ConnectionState = SteamConnectionState.Connected;
                        EventConnected.Invoke();

                        //if (BufferedData.Count > 0)
                        //{
                        //    Console.WriteLine($"{BufferedData.Count} received before connection was established. Processing now.");
                        //    {
                        //        foreach (Action action in BufferedData)
                        //        {
                        //            action();
                        //        }
                        //    }
                        //}
                        return;

                    case ESteamNetworkingConnectionState.k_ESteamNetworkingConnectionState_ClosedByPeer:
                        Disconnect();
                        break;

                    default:
                        Console.WriteLine($"{this}({_connection}) Connection state changed: {connectionState}");
                        break;
                }
            }
        }
        private void OnReceiveData()
        {
            ISteamNetworkingSockets.Default.FlushMessagesOnConnection(_connection);
            foreach (var steamNetworkingMessage in ISteamNetworkingSockets.Default.ReceiveMessagesOnConnection(_connection))
            {
                switch (ConnectionState)
                {
                    case SteamConnectionState.Connected:
                        EventReceivedData.Invoke(steamNetworkingMessage.GetData());
                        break;

                    default:
                        Console.WriteLine($"Recived message from {this} with out connection");
                        //BufferedData.Add(() => OnReceivedData(data, ch));
                        break;
                }
            }
        }

        public P2PSocketSteamClient()
        {
            ISteamNetworkingSockets.Default.EventSteamNetConnectionStatusChangedCallback.AddListener(OnConnectionStatusChanged);
        }

        public override Task OnUpdateAsync(CancellationToken cancellationToken)
        {
            OnReceiveData();
            return base.OnUpdateAsync(cancellationToken);
        }

        public override string ToString() => $"{_connectionSteamID}({_connection}):{_connectionVirtualPort}";

        private int _connectionVirtualPort;
        private SteamID _connectionSteamID;
        private HSteamNetConnection _connection;
    }
}
