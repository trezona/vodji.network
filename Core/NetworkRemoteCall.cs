﻿//using System;
//using System.Collections.Generic;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Core.Enums;
//using Vodji.Network.Data;
//using Vodji.Shared.Extensions;

//namespace Vodji.Network.Core
//{
//    public delegate void NetworkRemoteDelegate(object NetworkBehaviour, NetworkReader reader, object NetworkConnectionToClient);
//    public class NetworkRemoteCall
//    {
//        public static int GetFuncHash(Type funcClass, string funcName)
//        {
//            unchecked
//            {
//                int hash = funcClass.FullName.GetStableHashCode();
//                return hash * 503 + funcName.GetStableHashCode();
//            }
//        }

//        public int AddHandler(Type funcClass, string funcName, NetworkRemoteType funcType, NetworkRemoteDelegate funcDelegate, bool ignoreAuthority = false)
//        {
//            int funcHash = GetFuncHash(funcClass, funcName);
//            if(IsExists(funcClass, funcType, funcDelegate, funcHash))
//            {
//                return funcHash;
//            }

//            var remoteInvoker = new NetworkRemoteInvoker
//            {
//                FuncClass = funcClass,
//                FuncType = funcType,
//                FuncDelegate = funcDelegate,
//                IgnoreAuthority = ignoreAuthority
//            };
//            _handlers[funcHash] = remoteInvoker;

//            string ingoreAuthorityMessage = funcType == NetworkRemoteType.CMD ? $" IgnoreAuthority:{ignoreAuthority}" : "";
//            LoggingManager.GetLogger().LogInformation($"RegisterDelegate hash: {funcHash} invokerType: {funcType} method: {funcDelegate.Method.Name} {ingoreAuthorityMessage}");

//            return funcHash;
//        }
//        public int AddHandler(Type funcClass, string funcName, NetworkRemoteDelegate funcDelegate, bool ignoreAuthority)
//        {
//            return AddHandler(funcClass, funcName, NetworkRemoteType.CMD, funcDelegate, ignoreAuthority);
//        }
//        public int AddHandlerRPC(Type funcClass, string funcName, NetworkRemoteDelegate funcDelegate)
//        {
//            return AddHandler(funcClass, funcName, NetworkRemoteType.RPC, funcDelegate);
//        }

//        public void RemoveHandler(int funcHash) => _handlers.Remove(funcHash);
//        public bool Invoke(int funcHash, NetworkRemoteType funcType, NetworkReader reader, object NetworkBehaviour, object NetworkConnectionToClient = null)
//        {
//            if (TryGetValue(funcHash, funcType, out var remoteInvoker) && remoteInvoker.FuncClass.IsInstanceOfType(NetworkBehaviour))
//            {
//                remoteInvoker.FuncDelegate(NetworkBehaviour, reader, NetworkConnectionToClient);
//                return true;
//            }

//            return false;
//        }

//        private bool TryGetValue(int funcHash, NetworkRemoteType funcType, out NetworkRemoteInvoker remoteInvoker)
//        {
//            if (_handlers.TryGetValue(funcHash, out remoteInvoker) && remoteInvoker != null && remoteInvoker.FuncType == funcType)
//            {
//                return true;
//            }

//            // debug message if not found, or null, or mismatched type
//            // (no need to throw an error, an attacker might just be trying to
//            //  call an cmd with an rpc's hash)
//            LoggingManager.GetLogger().LogInformation("GetInvokerForHash hash:" + funcHash + " not found");

//            return false;
//        }
//        private NetworkRemoteDelegate GetDelegate(int cmdHash)
//        {
//            if (_handlers.TryGetValue(cmdHash, out var remoteInvoker))
//            {
//                return remoteInvoker.FuncDelegate;
//            }
//            return null;
//        }

//        /*static CommandInfo GetCommandInfo(int cmdHash, NetworkBehaviour invokingType)
//{
//    if (GetInvokerForHash(cmdHash, MirrorInvokeType.Command, out Invoker invoker) && invoker.invokeClass.IsInstanceOfType(invokingType))
//    {
//        return new CommandInfo
//        {
//            ignoreAuthority = invoker.cmdIgnoreAuthority
//        };
//    }
//    return default;
//}*/

//        private bool IsExists(Type funcClass, NetworkRemoteType funcType, NetworkRemoteDelegate funcDelegate, int funcHash)
//        {
//            if (_handlers.ContainsKey(funcHash))
//            {
//                // something already registered this hash
//                NetworkRemoteInvoker oldInvoker = _handlers[funcHash];
//                if (oldInvoker.Equals(funcClass, funcType, funcDelegate))
//                {
//                    // it's all right,  it was the same function
//                    return true;
//                }

//                LoggingManager.GetLogger().LogError($"Function {oldInvoker.FuncClass}.{oldInvoker.FuncDelegate.Method.Name} and {funcClass}.{funcDelegate.Method.Name} have the same hash.  Please rename one of them");
//            }

//            return false;
//        }
//        private readonly Dictionary<int, NetworkRemoteInvoker> _handlers = new Dictionary<int, NetworkRemoteInvoker>();
//    }
//}