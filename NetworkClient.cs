﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Vodji.Network.Components;
using Vodji.Network.Components.Interfaces;
using Vodji.Network.Provider.Interfaces;
using Vodji.Shared.Background;

namespace Vodji.Network
{
    public class NetworkClient : JobBackground, IDisposable
    {
        public INetworkConnection NetworkConnection { get; }
        public NetworkMessageHandler<INetworkConnection> NetworkMessageHandler { get; set; }
        public ConnectionState ConnectionState { get; protected set; }

        public NetworkClient(INetworkBaseClient networkClient)
        {
            NetworkConnection = new NetworkConnection(networkClient);
            {
                networkClient.EventConnected.AddListener(OnConnected);
                networkClient.EventDisconnected.AddListener(OnDisconnected);
                networkClient.EventReceivedData.AddListener(OnReceivedData);
            }
        }

        public void ConnectTo(INetworkAddress networkAddress)
        {
            ConnectionState = ConnectionState.Connecting;
        }

        public void Disconnect()
        {
            ConnectionState = ConnectionState.Closed;
            if (NetworkConnection is not null)
            {
                NetworkConnection.Disconnect();
                //NetworkConnection = null;
            }
        }

        private void OnConnected()
        {
            if (NetworkConnection is not null)
            {
                ConnectionState = ConnectionState.Open;
                return;
            }

            Console.WriteLine("Skipped Connect message handling because connection is null.");
        }

        private void OnReceivedData(byte[] bytesRecived)
        {
            if (NetworkConnection is not null)
            {
                NetworkConnection.OnReceivedData(bytesRecived);
                return;
            }

            Console.WriteLine("Skipped Data message handling because connection is null.");
        }

        private void OnDisconnected()
        {
            if (ConnectionState == ConnectionState.Closed) return;
            ConnectionState = ConnectionState.Closed;
        }

        public void SendMessage<T>(T msg, int channel) where T : INetworkMessage
        {
            if (NetworkConnection is not null)
            {
                if (ConnectionState == ConnectionState.Open)
                {
                    NetworkConnection.SendMessage(msg, channel);
                    return;
                }
                Console.WriteLine("NetworkClient Send when not connected to a server");
                return;
            }

            Console.WriteLine("NetworkClient Send with no connection");
        }

        public override Task OnUpdateAsync(CancellationToken cancellationToken)
        {
            if (ConnectionState == ConnectionState.Open)
            {

            }
            return base.OnUpdateAsync(cancellationToken);
        }

        public override void Dispose()
        {
            NetworkMessageHandler.Clear();
            base.Dispose();
        }
    }

}
