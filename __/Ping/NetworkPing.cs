﻿// using System;
// using System.Diagnostics;
// using Vodji.Network.Components;
// using Vodji.Network.Ping.Messages;
// using Vodji.Shared.Logging;
//
// namespace Vodji.Network.Ping
// {
//     public class NetworkPing
//     {
//         public NetworkPing()
//         {
//             _stopwatch.Start();
//             pingFrequency = 10;
//             pingWindowSize = 10;
//             
//             _rtt = new NetworkAveragePing(pingWindowSize);
//             _offset = new NetworkAveragePing(pingWindowSize);
//         }
//
//         public float pingFrequency { get; }  // Как часто посылаем сообщения о пинге. Используется для расчета сет. пинга и RRT
//         public int pingWindowSize { get; } // Средний результат пинга
//
//         public double LocalTime() => _stopwatch.Elapsed.TotalSeconds;
//
//         public void Reset()
//         {
//             _rtt = new NetworkAveragePing(pingWindowSize);
//             _offset = new NetworkAveragePing(pingWindowSize);
//             _offsetMin = double.MinValue;
//             _offsetMax = double.MaxValue;
//         }
//
//         // public void OnClientPing(INetworkIdentity networkIdentity)
//         // {
//         //     if (!(LocalTime() - _lastPingTime >= pingFrequency)) return;
//         //     LogManager.GetLogger().LogTrace("OnClientPingMessage");
//         //     // networkIdentity.Send(new NetworkPingMessage(LocalTime()));
//         //     _lastPingTime = LocalTime();
//         // }
//         // public void OnServerPong(INetworkIdentity networkIdentity, NetworkPingMessage msg)
//         // {
//         //     LogManager.GetLogger().LogTrace("OnPingServerMessage");
//         //
//         //     var pongMsg = new NetworkPongMessage
//         //     {
//         //         clientTime = msg.clientTime,
//         //         serverTime = LocalTime()
//         //     };
//         //     
//         //     // networkIdentity.Send(pongMsg);
//         // }
//         public void OnClientPong(NetworkPongMessage msg)
//         {
//             LogManager.GetLogger().LogTrace($@"OnClientPongMessage");
//             double now = LocalTime();
//
//             // how long did this message take to come back
//             double newRtt = now - msg.clientTime;
//             _rtt.Add(newRtt);
//
//             double newOffset = now - newRtt * 0.5f - msg.serverTime;
//             double newOffsetMin = now - newRtt - msg.serverTime;
//             double newOffsetMax = now - msg.serverTime;
//             _offsetMin = Math.Max(_offsetMin, newOffsetMin);
//             _offsetMax = Math.Min(_offsetMax, newOffsetMax);
//
//             if (_offset.Value < _offsetMin || _offset.Value > _offsetMax)
//             {
//                 // the old offset was offrange,  throw it away and use new one
//                 _offset = new NetworkAveragePing(pingWindowSize);
//                 _offset.Add(newOffset);
//             }
//             else if (newOffset >= _offsetMin || newOffset <= _offsetMax)
//             {
//                 // new offset looks reasonable,  add to the average
//                 _offset.Add(newOffset);
//             }
//         }
//         
//         public double time => LocalTime() - _offset.Value;
//         public double timeVar => _offset.Var;
//         public double timeSd => Math.Sqrt(timeVar);
//         public double offset => _offset.Value;
//         public double rtt => _rtt.Value;
//         public double rttVar => _rtt.Var;
//         public double rttSd => Math.Sqrt(rttVar);
//         
//         private readonly Stopwatch _stopwatch = new Stopwatch();
//         private double _offsetMin = double.MinValue;
//         private double _offsetMax = double.MaxValue;
//         private double _lastPingTime;
//         private NetworkAveragePing _rtt;
//         private NetworkAveragePing _offset;
//     }
// }