﻿// using Vodji.Network.Components;
// using Vodji.Network.Other;
//
// namespace Vodji.Network.Ping.Messages
// {
//     public class NetworkPingMessage// : INetworkMessage
//     {
//         public double clientTime;
//         public NetworkPingMessage(double clientTime) => this.clientTime = clientTime;
//         public void Deserialize(INetworkReader reader) => reader.Read(out clientTime);
//
//         public void Serialize(INetworkWriter writer) => writer.Write(clientTime);
//     }
// }