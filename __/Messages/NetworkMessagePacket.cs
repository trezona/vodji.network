﻿//using System.Collections.Generic;
//using Vodji.Network.libNet.Messages;

//namespace Vodji.Network.Messages
//{
//    public class NetworkMessagePacket : INetworkMessage
//    {
//        // Total (12 Bytes)
//        // Header (5 Bytes)
//        public NetworkPacketType PacketType { get; set; } // 1 Byte
//        public ushort Connection { get; set; } // 2 Byte
//        public ushort Sequence { get; set; } // 2 Byte
//        public byte SequenceChannel { get; set; } // 1 Byte
//        public byte ChannelId { get; set; } // 1 Byte
        
//        // Fragment (7 Bytes)
//        public bool IsFragmented { get; set; } // 1 Byte
//        public ushort FragmentId { get; set; } // 2 Byte
//        public ushort FragmentPart { get; set; } // 2 Byte
//        public ushort FragmentsTotal { get; set; } // 2 Byte
        
//        // Data
//        public byte[] Data { get; set; }
        
//        // Delivery
//        // public object ObjectData { get; set; }

//        public void Deserialize(INetworkReader reader)
//        {
//            PacketType = (NetworkPacketType) reader.Read(out byte _);
//            Connection = reader.Read(out ushort _);
//            Sequence = reader.Read(out ushort _);
//            SequenceChannel = reader.Read(out byte _);
//            ChannelId = reader.Read(out byte _);
            
//            IsFragmented = reader.Read(out bool _);
//            if (IsFragmented)
//            {
//                FragmentId = reader.Read(out ushort _);
//                FragmentPart = reader.Read(out ushort _);
//                FragmentsTotal = reader.Read(out ushort _);
//            }

//            Data = reader.ReadBytesAndSize(out byte[] _);
//            // ObjectData = reader.Read(out object _);
//        }

//        public void Serialize(INetworkWriter writer)
//        {
//            writer.Write((byte)PacketType);
//            writer.Write(Connection);
//            writer.Write(Sequence);
//            writer.Write(SequenceChannel);
//            writer.Write(ChannelId);
            
//            writer.Write(IsFragmented);
//            if (IsFragmented)
//            {
//                writer.Write(FragmentId);
//                writer.Write(FragmentPart);
//                writer.Write(FragmentsTotal);
//            }
            
//            writer.WriteBytesAndSize(Data, 0, Data.Length);
//            // writer.Write(ObjectData);
//        }

//        public NetworkMessagePacket()
//        {
//            PacketType = NetworkPacketType.Data;
//            Data = new byte[0];
//        }
//    }
//}