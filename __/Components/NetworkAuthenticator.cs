﻿// using System;
// using Vodji.Network.Utils;
//
// namespace Vodji.Network
// {
//     [Serializable] public class EventNetworkConnection : NetworkEvent<NetworkConnection> { }
//     public abstract class NetworkAuthenticator
//     {
//         public EventNetworkConnection OnServerAuthenticated = new EventNetworkConnection();
//         internal abstract void OnServerStarted();
//         internal abstract void OnServerAuthenticate(NetworkConnection conn);
//         internal abstract void OnServerAuthenticateInternal(NetworkConnection conn);
//         
//         public EventNetworkConnection OnClientAuthenticated = new EventNetworkConnection();
//         internal abstract void OnClientStarted();
//         internal abstract void OnClientAuthenticate(NetworkConnection conn);
//         internal abstract void OnClientAuthenticateInternal(NetworkConnection conn);
//     }
// }