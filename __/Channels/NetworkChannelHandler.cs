﻿//using System.Collections.Generic;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Channels.Interfaces;
//using Vodji.Network.Core;
//using Vodji.Network.Core.Interfaces;
//using Vodji.Network.Data;
//using Vodji.Network.Identity.Interfaces;
//using Vodji.Network.Utils;

//namespace Vodji.Network.Channels
//{
//    public class NetworkChannelHandler : INetworkChannelHandler
//    {
//        public NetworkChannelHandler(INetworkIdentity networkIdentity = null) : this(networkIdentity, null) { }
//        public NetworkChannelHandler(INetworkIdentity networkIdentity, INetworkChannelHandler channelHandler)
//        {
//            this.networkIdentity = networkIdentity;
//            channels = channelHandler == null ? new Dictionary<int, INetworkChannel>() : channelHandler.Channels;
//        }

//        public int AddChannel<TChannel>(INetworkChannelOptions channelOptions = null) where TChannel : INetworkChannel, new()
//        {
//            var channel = NetworkHelper.GetNetworkId<TChannel>();
//            var objChannel = new TChannel()
//            {
//                ChannelOptions = channelOptions
//            };

//            if (!channels.TryAdd(channel, objChannel))
//            {
//                LoggingManager.GetLogger().LogWarning($"Channel {typeof(TChannel).Name} #{channel} not-created.");
//                return default;
//            }

//            LoggingManager.GetLogger().LogWarning($"Channel {typeof(TChannel).Name} #{channel} created.");
//            return channel;
//        }

//        public bool OnRecivedChannel(byte[] bytes)
//        {
//            var networkReader = new NetworkReader(bytes);
//            ushort channel = networkReader.Read(typeof(ushort)); // read Channel
//            /*if(channels.TryGetValue(channel, out INetworkChannel networkChannel))
//            {
//                LoggingManager.GetLogger().LogWarning($"Channel #{channel} data recived.");
//                networkChannel.OnInMessage(networkReader, networkIdentity); // read Channel Info
//                return true;
//            }*/

//            //LoggingManager.GetLogger().LogWarning($"Unknown channel: #{channel}");
//            return false;
//        }

//        public bool RemoveChannel<TChannel>() where TChannel : INetworkChannel
//        {
//            var channel = NetworkHelper.GetNetworkId<TChannel>();
//            if(!channels.ContainsKey(channel))
//            {
//                LoggingManager.GetLogger().LogWarning($"Not exist channel: #{channel}");
//                return false;
//            }

//            return channels.Remove(channel);
//        }

//        public bool OnSendingChannel(byte[] bytes, ushort channel)
//        {
//            LoggingManager.GetLogger().LogTrace($"Try send channel {channel}");
//            if (channels.TryGetValue(channel, out INetworkChannel networkChannel))
//            {
//                LoggingManager.GetLogger().LogTrace($"Find channel {networkChannel.GetType().Name}");
//                INetworkChannelPacket channelPacket = new NetworkChannelPacket
//                {
//                    Channel = channel,
//                    Payload = bytes
//                };

//                networkChannel.OnOutMessage(channelPacket, networkIdentity); // Write ChannelInfo
//                return true;
//            }

//            LoggingManager.GetLogger().LogWarning($"Unknown channel: #{channel}");
//            return false;
//        }

//        protected INetworkIdentity networkIdentity;
//        protected Dictionary<int, INetworkChannel> channels;

//        public Dictionary<int, INetworkChannel> Channels => channels;
//    }
//}
