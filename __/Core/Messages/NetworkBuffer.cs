﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Vodji.Network.libNet.Messages
{
    public partial class NetworkBuffer
    {
        public const int MaxStringLength = 1024 * 30;
        
        public NetworkBuffer()
        {
            _buffer = new byte[1500]; // MTU 1500
            _bufferString = new byte[MaxStringLength];
            _utf8Encoding = new UTF8Encoding(false, true);
        }

        public NetworkBuffer(byte[] bytes) : this() => _bufferArraySegment = new ArraySegment<byte>(bytes);
        public NetworkBuffer(ArraySegment<byte> arraySegment) : this() => _bufferArraySegment = arraySegment;

        public void Reset() => _bufferLength = _bufferPosition = 0;

        public int Position
        {
            get => _bufferPosition;
            set { EnsureLength(_bufferPosition = value); }
        }
        
        public int Length => _bufferLength;
        public void SetLength(int newLength)
        {
            int oldLength = _bufferLength;
            EnsureLength(newLength);

            // zero out new length
            if (oldLength < newLength)
            {
                Array.Clear(_buffer, oldLength, newLength - oldLength);
            }

            _bufferLength = newLength;
            _bufferPosition = Math.Min(_bufferPosition, _bufferLength);
        }
        
        public ArraySegment<byte> ToArraySegment() => new ArraySegment<byte>(_buffer, 0, _bufferLength);
        public byte[] ToArray()
        {
            byte[] data = new byte[_bufferLength];
            Array.ConstrainedCopy(_buffer, 0, data, 0, _bufferLength);
            return data;
        }
        
        protected void EnsureLength(int value)
        {
            if (_bufferLength >= value) return;
            EnsureCapacity(_bufferLength = value);
        }
        
        protected void EnsureCapacity(int value)
        {
            if (_buffer.Length >= value) return;
            int capacity = Math.Max(value, _buffer.Length * 2);
            Array.Resize(ref _buffer, capacity);
        }

        private byte[] _buffer;
        private byte[] _bufferString;
        private ArraySegment<byte> _bufferArraySegment;
        private UTF8Encoding _utf8Encoding;
        private int _bufferLength;
        private int _bufferPosition;
    }
}