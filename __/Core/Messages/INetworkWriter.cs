﻿using System;

namespace Vodji.Network.libNet.Messages
{
    public interface INetworkWriter
    {
        // public void Write(object value);
        public void Write(string value);
        public void Write(Uri uri);

        /// <summary> 1 Byte </summary>
        public void Write(byte value);
        public void Write(sbyte value);
        public void Write(bool value);

        /// <summary> 2 Byte </summary>
        public void Write(ushort value);
        public void Write(short value);
        public void Write(char value);
        
        /// <summary> 4 Byte </summary>
        public void Write(int value);
        public void Write(uint value);
        public void Write(float value);

        /// <summary> 8 Byte </summary>
        public void Write(long value);
        public void Write(ulong value);
        public void Write(double value);

        /// <summary> 16 Byte </summary>
        public void Write(decimal value);
        

        public void WriteBytes(byte[] bytes, int offset, int count);
        public void WriteBytesAndSize(byte[] buffer, int offset, int count);
        
        public int Length { get; }
    }
}