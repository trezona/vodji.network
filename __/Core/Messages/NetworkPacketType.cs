﻿namespace Vodji.Network.libNet.Messages
{
    public enum NetworkPacketType : byte
    {
        Data,
        Request,
        Channeled,
        Ack,
        Ping,
        Pong,
        ConnectRequest,
        ConnectAccept,
        Disconnect,
        UnconnectedMessage,
        NatIntroductionRequest,
        NatIntroduction,
        NatPunchMessage,
        MtuCheck,
        MtuOk,
        Broadcast,
        Merged,
        ShutdownOk,
        PeerNotFound,
        InvalidProtocol
    }
}