﻿using System;

namespace Vodji.Network.libNet.Messages
{
    public interface INetworkReader
    {
        // object Read(out object value);
        byte Read(out byte value);
        ulong Read(out ulong value);
        uint Read(out uint value); // UInt32
        public ushort Read(out ushort value); // UInt16
        public float Read(out float value);
        public double Read(out double value);
        public decimal Read(out decimal value);
        public string Read(out string value);
        public byte[] Read(out byte[] value);
        public int Read(out int value);
        public long Read(out long value); // Int64
        public sbyte Read(out sbyte value);
        public char Read(out char value);
        public bool Read(out bool value);
        public short Read(out short value); // Int16
        public Guid Read(out Guid value);
        public Uri Read(out Uri value);

        public byte[] ReadBytes(int count);
        public byte[] ReadBytesAndSize(out byte[] value);
    }
}