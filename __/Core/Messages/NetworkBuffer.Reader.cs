﻿//using System;
//using System.IO;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Utils;

//namespace Vodji.Network.libNet.Messages
//{
//    public partial class NetworkBuffer : INetworkReader
//    {
//        public object Read(out object value)
//        {
//            value = null;
//            var size = Read(out ushort _);
//            if (size == 0) return value;

//            var bytes = ReadBytes(size);
//            var stream = new MemoryStream(bytes);
//            try
//            {
//                var formatter = new BinaryFormatter();
//                value = formatter.Deserialize(stream);
//            }
//            catch (SerializationException e)
//            {
//                LoggingManager.GetLogger().LogCritical(e, "Failed to serialize");
//            }
//            finally
//            {
//                stream.Close();
//            }

//            return value;
//        }

//        public byte Read(out byte value)
//        {
//            if (_bufferPosition + 1 > _bufferArraySegment.Count)
//            {
//                throw new EndOfStreamException("ReadByte out of range:" + ToString());
//            }

//            return value = _bufferArraySegment.Array?[_bufferArraySegment.Offset + _bufferPosition++] ?? default;
//        }
//        public ulong Read(out ulong value) // UInt64
//        {
//            value = 0;
//            value |= Read(out byte _);
//            value |= ((ulong)Read(out byte _)) << 8;
//            value |= ((ulong)Read(out byte _)) << 16;
//            value |= ((ulong)Read(out byte _)) << 24;
//            value |= ((ulong)Read(out byte _)) << 32;
//            value |= ((ulong)Read(out byte _)) << 40;
//            value |= ((ulong)Read(out byte _)) << 48;
//            value |= ((ulong)Read(out byte _)) << 56;
//            return value;
//        }
//        public uint Read(out uint value) // UInt32
//        {
//            value = 0;
//            value |= Read(out byte _);
//            value |= (uint)(Read(out byte _) << 8);
//            value |= (uint)(Read(out byte _) << 16);
//            value |= (uint)(Read(out byte _) << 24);
//            return value;
//        }
//        public ushort Read(out ushort value) // UInt16
//        {
//            value = 0;
//            value |= Read(out byte _);
//            value |= (ushort)(Read(out byte _) << 8);
//            return value;
//        }
//        public float Read(out float value)
//        {
//            var converter = new UIntFloat
//            {
//                intValue = Read(out uint _)
//            };
            
//            return value = converter.floatValue;
//        }
//        public double Read(out double value)
//        {
//            var converter = new UIntDouble
//            {
//                longValue = Read(out ulong _)
//            };
//            return value = converter.doubleValue;
//        }
//        public decimal Read(out decimal value)
//        {
//            var converter = new UIntDecimal
//            {
//                longValue1 = Read(out ulong _),
//                longValue2 = Read(out ulong _),
//            };
//            return value = converter.decimalValue;
//        }
//        public string Read(out string value)
//        {
//            // read number of bytes
//            ushort size = Read(out ushort _);
//            if (size == 0) return value = string.Empty;
            
//            int realSize = size - 1;
//            if (realSize >= MaxStringLength)
//            {
//                throw new EndOfStreamException("ReadString too long: " + realSize + ". Limit is: " + MaxStringLength);
//            }

//            var data = ReadBytes(realSize);
//            return value = _utf8Encoding.GetString(data, 0, realSize);
//        }

//        public byte[] Read(out byte[] value)
//        {
//            Read(out ushort size);
//            return value = ReadBytes(size);
//        }
//        public int Read(out int value) => value = (int)Read(out uint _);
//        public long Read(out long value) => value = (long)Read(out ulong _); // Int64
//        public sbyte Read(out sbyte value) => value = (sbyte)Read(out byte _);
//        public char Read(out char value) => value = (char)Read(out ushort _);
//        public bool Read(out bool value) => value = Read(out byte _) != 0;
//        public short Read(out short value) => value = (short)Read(out ushort _); // Int16
//        public Guid Read(out Guid value) => value = new Guid(ReadBytes(16));
//        public Uri Read(out Uri value) => value = new Uri(Read(out string _));

//        public byte[] ReadBytes(int count)
//        {
//            byte[] bytes = new byte[count];
//            ReadBytes(bytes, count);
//            return bytes;
//        }
//        public byte[] ReadBytesAndSize(out byte[] value)
//        {
//            ushort count = Read(out ushort _);
//            return value = count == 0 ? null : ReadBytes(checked((ushort)(count - 1)));
//        }
        
//        protected int ReadPacked(out int value)
//        {
//            uint data = ReadPacked(out uint _);
//            return value = (int)((data >> 1) ^ -(data & 1));
//        } 
//        protected uint ReadPacked(out uint value) => value = checked((uint)ReadPacked(out ulong _)); 
//        protected long ReadPacked(out long value)
//        {
//            ulong data = ReadPacked(out ulong _);
//            return value = ((long)(data >> 1)) ^ -((long)data & 1);
//        }
//        protected ulong ReadPacked(out ulong value)
//        {
//            byte a0 = Read(out byte _);
//            if (a0 < 241)
//            {
//                return value = a0;
//            }

//            byte a1 = Read(out byte _);
//            if (a0 >= 241 && a0 <= 248)
//            {
//                return value = 240 + ((a0 - (ulong)241) << 8) + a1;
//            }

//            byte a2 = Read(out byte _);
//            if (a0 == 249)
//            {
//                return value = 2288 + ((ulong)a1 << 8) + a2;
//            }

//            byte a3 = Read(out byte _);
//            if (a0 == 250)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16);
//            }

//            byte a4 = Read(out byte _);
//            if (a0 == 251)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24);
//            }

//            byte a5 = Read(out byte _);
//            if (a0 == 252)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32);
//            }

//            byte a6 = Read(out byte _);
//            if (a0 == 253)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40);
//            }

//            byte a7 = Read(out byte _);
//            if (a0 == 254)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48);
//            }

//            byte a8 = Read(out byte _);
//            if (a0 == 255)
//            {
//                return value = a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48) + (((ulong)a8) << 56);
//            }

//            throw new IndexOutOfRangeException("ReadPackedUInt64() failure: " + a0);
//        }
        
//        protected byte[] ReadBytes(byte[] bytes, int count)
//        {
//            // check if passed byte array is big enough
//            if (count > bytes.Length)
//            {
//                throw new EndOfStreamException("ReadBytes can't read " + count + " + bytes because the passed byte[] only has length " + bytes.Length);
//            }

//            var data = ReadBytesSegment(count);
//            Array.Copy(data.Array, data.Offset, bytes, 0, count);
//            return bytes;
//        }
        
//        protected ArraySegment<byte> ReadBytesSegment(int count)
//        {
//            // check if within buffer limits
//            if (_bufferPosition + count > _bufferArraySegment.Count)
//            {
//                throw new EndOfStreamException("ReadBytesSegment can't read " + count + " bytes because it would read past the end of the stream. " + ToString());
//            }

//            // return the segment
//            ArraySegment<byte> result = new ArraySegment<byte>(_bufferArraySegment.Array, _bufferArraySegment.Offset + _bufferPosition, count);
//            _bufferPosition += count;
//            return result;
//        }
//        protected ArraySegment<byte> ReadSegment(out ArraySegment<byte> value)
//        {
//            uint count = ReadPacked(out uint _);
//            return value = count == 0 ? default : ReadBytesSegment(checked((int)(count - 1u)));
//        }
//    }
//}