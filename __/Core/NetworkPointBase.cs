﻿//using System;
//using System.Collections.Generic;
//using System.Net;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.libNet.Channels;
//using Vodji.Network.libNet.Messages;
//using Vodji.Network.Messages;

//namespace Vodji.Network.libNet
//{
//    public abstract class NetworkPointBase
//    {
//        public NetworkConnectionStatus  Status              { get; }
//        public _NetworkConnection        Connection          { get; }
//        public NetworkHandler           Handler             { get; }
//        public IPEndPoint               ClientEndPoint      { get; }
        
//        protected NetworkPointBase(_NetworkConnection networkConnection, IPEndPoint clientEndPoint)
//        {
//            Status = NetworkConnectionStatus.Connected;
//            Connection = networkConnection;
//            ClientEndPoint = clientEndPoint;
//            Handler = new NetworkHandler();

//            _channels = new Dictionary<int, INetworkChannel>
//            {
//                [NetworkUnreliableChannel.CHANNEL_ID]       = new NetworkUnreliableChannel(this),
//                [NetworkReliableOrderedChannel.CHANNEL_ID]  = new NetworkReliableOrderedChannel(this)
//            };
//        }

//        public virtual void OnRecivedData(byte[] bytes, int channelId = 0)
//        {
//            INetworkReader networkReader = new NetworkBuffer(bytes);
//            if (networkReader.UnpackMessage(out var msgType))
//            {
//                // logging
//                LoggingManager.GetLogger().LogTrace($"ConnectionRecv {this} msgType: {msgType} bytes: {bytes.Length} FromChannel: {(channelId > 0)}");

//                // try to invoke the handler for that message
//                //if (Handler.Invoke(msgType, networkReader, this as NetworkPoint, channelId))
//                //{
//                //    // _lastMessageTime = DateTime.Now;;
//                //}
//            }
//            else
//            {
//                LoggingManager.GetLogger().LogTrace("Closed connection: " + this + ". Invalid message header.");
//                // Disconnect();
//            }
//        }

//        // Отправка модели сообщения, с кодировкой в байты
//        public NetworkMessageResult SendMessage(INetworkMessage msg, int channelId = 0, int sequenceChannel = 0)
//        {
//            if (msg == null)
//            {
//                LoggingManager.GetLogger().LogWarning("Skipped send message handling because message empty.");
//                return NetworkMessageResult.Dropped;
//            }

//            SendMessage(msg.Pack(), channelId, sequenceChannel);
//            return NetworkMessageResult.Queued;
//        }
        
//        // Отправка байтов
//        public NetworkMessageResult SendMessage(byte[] bytes, int channelId = 0, int sequenceChannel = 0)
//        {
//            // Есть подключение ?
//            if (Status != NetworkConnectionStatus.Connected)
//            {
//                LoggingManager.GetLogger().LogWarning($"Send when not connected to a server");
//                return NetworkMessageResult.NotConnected;
//            }
            
//            // Доп.Канал имеет положительное число ?
//            if (0 > sequenceChannel)
//            {
//                LoggingManager.GetLogger().LogWarning($"Delivery channel {channelId} cannot use sequence channels other than 0!");
//                return NetworkMessageResult.Dropped;
//            }
            
//            // Проверка на валидность
//            if (ValidatePacketSize(bytes, channelId))
//            {
//                if(!_channels.TryGetValue(channelId, out var channel))
//                {
//                    LoggingManager.GetLogger().LogWarning($"Delivery channel {channelId} not exist!");
//                    return NetworkMessageResult.Dropped;
//                }


//                LoggingManager.GetLogger().LogTrace($"Queued Data message bytes {bytes.Length}.");
//                channel.SendMessage(bytes, sequenceChannel);
//                return NetworkMessageResult.Queued;
//            }

//            // Если сообщение, не прошло проверку. Выдаем уведомление, Сообщение дропаем
//            LoggingManager.GetLogger().LogTrace($"Skipped Data message not validate.");
//            return NetworkMessageResult.Dropped;
//        }
        
//        // Метод проверки на валидность самого сообщения
//        private bool ValidatePacketSize(IReadOnlyCollection<byte> bytes, int channelId)
//        {
//            // Провека, размер сообщение на MTU, Фрагментация не доступна
//            //if (bytes.Count > ClientMTU && NetworkConnection.Configuration.DropAboveMTU)
//            //{
//            //    LogManager.GetLogger().LogWarning("Cannot send packet larger than " + ClientMTU + " bytes");
//            //    return false;
//            //}
            
//            // Размер сообщения, больше 0
//            if (bytes.Count > 0) return true;

//            // Ошибка, нету данных в сообщении
//            LoggingManager.GetLogger().LogWarning("Cannot send zero bytes");
//            return false;
//        }

//        private NetworkSocket _networkSocket;
//        //private Dictionary<int, INetworkChannel> _channels;
//    }
//}