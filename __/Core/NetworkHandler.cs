﻿//using System;
//using System.Collections.Generic;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Identity;
//using Vodji.Network.libNet.Messages;

//namespace Vodji.Network.libNet
//{
//    public class NetworkHandler
//    {
//        public delegate void DelegateNetworkHandlers(NetworkPoint client, INetworkReader reader, int channel);
//        public Dictionary<int, DelegateNetworkHandlers> messageHandlers { get; set; }
        
//        public NetworkHandler() => messageHandlers = new Dictionary<int, DelegateNetworkHandlers>();

//        public bool AddListener<T>(Action<T> handler) where T : INetworkMessage, new() => AddListener((NetworkPoint _, T value) => handler(value));
//        public bool AddListener<T>(Action<NetworkPoint, T> messageHandler) where T : INetworkMessage, new()
//        {
//            int msgType = NetworkMessageExtension.GetNetworkId<T>();
            
//            var packMessage = InvokeMessage(messageHandler);
//            if (messageHandlers.TryAdd(msgType, packMessage))
//            {
//                LoggingManager.GetLogger().LogWarning($"Adding handler for {typeof(T).FullName}, id={msgType}.");
//                return true;
//            }

//            //Если замена является преднамеренной, используйте ReplaceListener.
//            LoggingManager.GetLogger().LogWarning($"If replacement is intentional, use ReplaceListener<{typeof(T).FullName}>.");
//            return false;
//        }
        
//        public bool ReplaceListener<T>(Action<T> handler) where T : INetworkMessage, new() => ReplaceListener((NetworkPoint _, T value) => handler(value));
//        public bool ReplaceListener<T>(Action<NetworkPoint, T> handler) where T : INetworkMessage, new()
//        {
//            int msgType = NetworkMessageExtension.GetNetworkId<T>();
//            if (messageHandlers.TryGetValue(msgType, out _))
//            {
//                messageHandlers[msgType] = InvokeMessage(handler);
//                LoggingManager.GetLogger().LogWarning($"Replacing handler {typeof(T).FullName}, id={msgType}.");
//                return true;
//            }

//            //Обработчик отсуствует, необходимо добавить новый AddListener
//            LoggingManager.GetLogger().LogWarning($"The handler is missing, you need to add a new AddListener<{typeof(T).FullName}>");
//            return false;
//        }
        
//        public bool RemoveListener<T>() where T : INetworkMessage
//        {
//            int msgType = NetworkMessageExtension.GetNetworkId<T>();
//            if(messageHandlers.Remove(msgType))
//            {
//                LoggingManager.GetLogger().LogWarning($"The {typeof(T).FullName} handler removed.");
//                return true;
//            }

//            LoggingManager.GetLogger().LogWarning($"The {typeof(T).FullName} handler is missing, dont remove.");
//            return false;
//        }

//        // Full Invoke
//        public bool Invoke(int msgType, INetworkReader reader, NetworkPoint client, int channelId = 0)
//        {
//            if (messageHandlers.TryGetValue(msgType, out var msgDelegate))
//            {
//                msgDelegate(client, reader, channelId);
//                return true;
//            }
//            LoggingManager.GetLogger().LogWarning($"Unknown message ID {msgType}. May be due to no existing AddListener for this message.");
//            return false;
//        }

//        // LocalInvoke
//        public bool Invoke<T>(T msg, NetworkPoint client, int channelId) where T : INetworkMessage
//        {
//            var networkBuffer = new NetworkBuffer();
//            int msgType = msg.GetNetworkId();
//            msg.Pack(networkBuffer);
//            return Invoke(msgType, networkBuffer, client, channelId);
//        }
//        public bool Invoke(byte[] bytes, NetworkPoint client, byte channelId = 0)
//        {
//            var reader = new NetworkBuffer(bytes);
//            if (reader.UnpackMessage(out int msgType))
//            {
//                // LogManager.GetLogger().LogTrace($"msgType:{msgType} Received: {bytes.Length} via UDP from {identity.GetType().Name}");
//                return Invoke(msgType, reader, client, channelId);
//            }

//            LoggingManager.GetLogger().LogError($"Closed connection: {client}. Invalid message header.");
//            return false;
//            // DisconnectInfo();
//        }
        
//        private static DelegateNetworkHandlers InvokeMessage<T>(Action<NetworkPoint, T> handler)
//            where T : INetworkMessage, new() => 
//            (identity, reader, channelId) =>
//        {
//            // protect against DOS attacks if attackers try to send invalid
//            // data packets to crash the server/client. there are a thousand
//            // ways to cause an exception in data handling:
//            // - invalid headers
//            // - invalid message ids
//            // - invalid data causing exceptions
//            // - negative ReadBytesAndSize prefixes
//            // - invalid utf8 strings
//            // - etc.
//            //
//            // let's catch them all and then disconnect that connection to avoid
//            // further attacks.
//            T message = default;
//            try
//            {
//                //     if (requireAuthenication && !conn.isAuthenticated)
//                //     {
//                //         // message requires authentication, but the connection was not authenticated
//                //         logger.LogWarning($"Closing connection: {conn}. Received message {typeof(T)} that required authentication, but the user has not authenticated yet");
//                //         conn.Disconnect();
//                //         return;
//                //     }
//                //
//                //     // if it is a value type, just use defult(T)
//                //     // otherwise allocate a new instance
//                message ??= new T();
//                message.Deserialize(reader);
//            }
//            catch (Exception exception)
//            {
//                LoggingManager.GetLogger().LogError("Closed connection: " + identity +
//                                ". This can happen if the other side accidentally (or an attacker intentionally) sent invalid data. Reason: " +
//                                exception);
//                // identity.Disconnect();
//                return;
//            }
//            // finally
//            // {
//            //     // TODO: Figure out the correct channel
//            //     // NetworkDiagnostics.OnReceive(message, channelId, reader.Length);
//            // }

//            handler.Invoke(identity, message);
//        };
//    }
//}