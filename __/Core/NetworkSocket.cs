﻿//using System;
//using System.Net;
//using System.Net.Sockets;
//using System.Threading;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Core;

//namespace Vodji.Network.libNet
//{
//    public abstract class NetworkSocket : IDisposable
//    {
//        public NetworkSocketConfiguration Configuration { get; }
//        public UdpClient UdpClient { get; protected set; }
//        public Socket Socket => UdpClient.Client;

//        public NetworkSocket() : this(new NetworkSocketConfiguration()) { }
//        public NetworkSocket(NetworkSocketConfiguration socketConfiguration)
//        {
//            LoggingManager.GetLogger<NetworkSocket>().LogDebug("Network started");
//            Configuration = socketConfiguration ?? new NetworkSocketConfiguration();
//            BindSocket(false);
//            BeginReceive();
//        }
        
//        private void BindSocket(bool reuseAdress)
//        {
//            using var mutex = new Mutex(false, "Global\\NetworkSocketBind");
//            try
//            {
//                mutex.WaitOne();
//                UdpClient ??= new UdpClient(Configuration.SelfAddress.AddressFamily);

//                if (reuseAdress)
//                {
//                    Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
//                }

//                Socket.ReceiveBufferSize = Configuration.ReceiveBufferSize;
//                Socket.SendBufferSize = Configuration.SendBufferSize;
//                Socket.Blocking = false;

//                if (Configuration.DualMode)
//                {
//                    if (Configuration.SelfAddress.AddressFamily != AddressFamily.InterNetworkV6)
//                    {
//                        // Конфигурация указывает двойной режим, но не использует локальный адрес IPv6; двойной режим не будет работать.
//                        LoggingManager.GetLogger().LogWarning("Configuration specifies Dual Mode but does not use IPv6 local address; Dual mode will not work.");
//                    }
//                    else Socket.DualMode = Configuration.DualMode;
//                }

//                var selfAdress = new IPEndPoint(Configuration.SelfAddress, Configuration.Port);
//                Socket.Bind(selfAdress);

//                try
//                {
//                    const uint IOC_IN = 0x80000000;
//                    const uint IOC_VENDOR = 0x18000000;
//                    const uint SIO_UDP_CONNRESET = (IOC_IN | IOC_VENDOR | 12);
//                    Socket.IOControl(unchecked((int)SIO_UDP_CONNRESET), new[] { Convert.ToByte(false) }, null);
//                }
//                catch (Exception e)
//                {
//                    LoggingManager.GetLogger().LogError(e, "SIO_UDP_CONNRESET not supported on this platform");
//                }
//            }
//            finally
//            {
//                mutex.ReleaseMutex();
//            }

//            Configuration.SelfAddress = ((IPEndPoint)Socket.LocalEndPoint).Address;
//            Configuration.Port = ((IPEndPoint)Socket.LocalEndPoint).Port;
//            Configuration.Lock();

//            var endPoint = new IPEndPoint(Configuration.SelfAddress, Configuration.Port);
//            LoggingManager.GetLogger().LogDebug($"Socket bound to {endPoint} Listen: {Socket.IsBound}");
//        }

//        #region Recived Data
//        protected abstract void OnRecivedData(IPEndPoint clientPoint, byte[] recivedBytes);
//        private void ReceiveData(IAsyncResult ar)
//        {
//            var clientPoint = new IPEndPoint(IPAddress.Any, 0);
//            var recivedBytes = UdpClient.EndReceive(ar, ref clientPoint);

//            LoggingManager.GetLogger().LogDebug($"Received {recivedBytes.Length} bytes from {clientPoint}");
//            OnRecivedData(clientPoint, recivedBytes);

//            BeginReceive();
//        }
//        private void BeginReceive() => UdpClient.BeginReceive(ReceiveData, this);
//        #endregion

//        #region Send Data
//        //public bool SendMTUPacket(byte[] bytes, int numBytes, IPEndPoint remoteEndPoint, out bool connectionReset)
//        //{
//        //    connectionReset = false;
//        //    try
//        //    {
//        //        UdpClient.DontFragment = true;
//        //        SendPacket(bytes, numBytes, remoteEndPoint, out connectionReset);
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        LoggingManager.GetLogger().LogError(e, e.Message);
//        //        return false;
//        //    }
//        //    finally
//        //    {
//        //        UdpClient.DontFragment = false;
//        //    }

//        //    return true;
//        //}
//        public SocketError SendPacket(byte[] bytes, IPEndPoint clientPoint) => SendPacket(bytes, bytes.Length, clientPoint);
//        public SocketError SendPacket(byte[] bytes, int numBytes, IPEndPoint clientPoint)
//        {
//            // NetworkStatistics.PacketSent(numBytes, numMessages);
//            try
//            {
//                if (clientPoint.Address.Equals(IPAddress.Broadcast))
//                {
//                    Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
//                }

//                UdpClient.BeginSend(bytes, numBytes, clientPoint, SendData, new Tuple<int, IPEndPoint>(numBytes, clientPoint));
//            }
//            catch (SocketException socketException)
//            {
//                switch (socketException.SocketErrorCode)
//                {
//                    case SocketError.WouldBlock: // send buffer full?
//                        LoggingManager.GetLogger().LogWarning("Increase Buffer in NetworkConfiguration");
//                        return SocketError.WouldBlock;

//                    default:
//                        LoggingManager.GetLogger().LogError(socketException, "Failed to send packet: (" + socketException.SocketErrorCode + ") ");
//                        return socketException.SocketErrorCode;
//                }
//            }
//            catch (Exception e)
//            {
//                LoggingManager.GetLogger().LogError(e, "Failed to send packet: ");
//                return SocketError.Fault;
//            }
//            finally
//            {
//                if (clientPoint.Address.Equals(IPAddress.Broadcast))
//                {
//                    Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);
//                }
//            }

//            return SocketError.Success;
//        }
//        protected abstract void OnSendData(int sentNumBytes, IPEndPoint clientPoint);
//        private void SendData(IAsyncResult ar)
//        {
//            var (numBytes, clientPoint) = ar.AsyncState as Tuple<int, IPEndPoint>;

//            var sentNumBytes = UdpClient.EndSend(ar);
//            if (numBytes != sentNumBytes)
//            {
//                LoggingManager.GetLogger().LogWarning($"Failed to send the full {numBytes} only {sentNumBytes} bytes sent in packet!");
//            }

//            LoggingManager.GetLogger().LogTrace($"Send {sentNumBytes} bytes via UDP to specifically client {clientPoint}.");
//            OnSendData(sentNumBytes, clientPoint);
//        }
//        #endregion

//        public void Dispose()
//        {
//            LoggingManager.GetLogger().LogDebug("Shutting down...");
//            try
//            {
//                if (Socket != null)
//                {

//                    try
//                    {
//                        Socket.Shutdown(SocketShutdown.Receive);
//                    }
//                    catch (Exception e)
//                    {
//                        LoggingManager.GetLogger().LogError(e, "Socket.Shutdown exception");
//                    }

//                    try
//                    {
//                        UdpClient.Close();
//                    }
//                    catch (Exception e)
//                    {
//                        LoggingManager.GetLogger().LogError(e, "UdpClient.Close exception");
//                    }
//                }
//            }
//            finally
//            {
//                UdpClient = null;
//                LoggingManager.GetLogger().LogDebug("Shutdown complete");
//            }

//            // m_connections.Clear();
//            // m_connectionLookup.Clear();
//            // m_handshakes.Clear();
//            GC.SuppressFinalize(this);
//        }
//    }
//}