﻿using System.Text;

namespace Vodji.Network.libNet
{
    public class NetworkStatistics
    {
		public NetworkStatistics(object networkConnection)
		{
			_networkConnection = networkConnection;
			Reset();
		}

		public void Reset()
		{
			SentPackets = 0;
			ReceivedPackets = 0;

			SentMessages = 0;
			ReceivedMessages = 0;
			ReceivedFragments = 0;

			SentBytes = 0;
			ReceivedBytes = 0;

			StorageBytesAllocated = 0;
		}

		public int SentPackets { get; internal set; }
		public int ReceivedPackets { get; internal set; }
		public int SentMessages{ get; internal set; }
		public int ReceivedMessages { get; internal set; }
		public int SentBytes { get; internal set; }
		public int ReceivedBytes { get; internal set; }
		public int ReceivedFragments { get; internal set; }
		public long StorageBytesAllocated { get; internal set; }
		
		internal void PacketSent(int numBytes, int numMessages)
		{
			SentPackets++;
			SentBytes += numBytes;
			SentMessages += numMessages;
		}

		internal void PacketReceived(int numBytes, int numMessages, int numFragments)
		{
			ReceivedPackets++;
			ReceivedBytes += numBytes;
			ReceivedMessages += numMessages;
			ReceivedFragments += numFragments;
		}

		public override string ToString()
		{
			var stringBuilder = new StringBuilder();
			// stringBuilder.AppendLine(_networkConnection.ConnectionsCount + " connections");

			stringBuilder.AppendLine("Sent " + SentBytes + " bytes in " + SentMessages + " messages in " + SentPackets + " packets");
			stringBuilder.AppendLine("Received " + ReceivedBytes + " bytes in " + ReceivedMessages + " messages (of which " + ReceivedFragments + " fragments) in " + ReceivedPackets + " packets");

			stringBuilder.AppendLine("Storage allocated " + StorageBytesAllocated + " bytes");
			return stringBuilder.ToString();
		}
		
		private readonly object _networkConnection;
    }
}