﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.libNet.Messages;
//using Vodji.Network.Messages;
//using Vodji.Shared;
//using Vodji.Shared.Background;

//// Добавить таймАут на входящее сообщение
//// Переполнение буффера в ожидании

//namespace Vodji.Network.libNet.Channels
//{
//    public class NetworkReliableOrderedMessage : NetworkMessagePacket { }
//    public class NetworkReliableOrderedChannel : TaskApplication, INetworkChannel
//    {
//        // ID = 5
//        public const int CHANNEL_ID = NetworkChannel.ReliableOrdered;

//        private NetworkPointBase _networkPointBase;
//        private int _windowSize;

//        public NetworkReliableOrderedChannel(NetworkPointBase networkPointBase)
//        {
//            networkPointBase.Handler.AddListener<NetworkReliableOrderedMessage>(OnRecivePacket);
//            // Setup
//            _networkPointBase = networkPointBase;
//            _windowSize = 256;//_networkPoint.NetworkConnection.Configuration.WindowSize;
            
//            // Sender
//            _queueMessages = new Queue<NetworkReliableOrderedMessage>();
//            _storeMessage = new Dictionary<int, NetworkPacketInfo>();
//            _lastSentTime = DateTime.Now;
            
//            // Recived
//            _recivedMessages = new Dictionary<int, INetworkMessage>();
            
//            // Common
//            TaskRun();
//        }

//        ~NetworkReliableOrderedChannel()
//        {
//            _networkPointBase.Handler.RemoveListener<NetworkReliableOrderedMessage>();
//        }


//        // ====================
//        // ------ Sender ------
//        // ====================

//        private const int TIMEOUT = 100;
//        public const int RETRY_COUNT = 40;
//        private Queue<NetworkReliableOrderedMessage> _queueMessages;
//        private Dictionary<int, NetworkPacketInfo> _storeMessage;
//        private ushort _sequenceSended;
//        private DateTime _lastSentTime;

//        public void SendMessage(byte[] bytes, int sequenceChannel = 0)
//        {
//            var packet = new NetworkReliableOrderedMessage
//            {
//                PacketType = NetworkPacketType.Data,
//                ChannelId = CHANNEL_ID,
//                SequenceChannel = (byte)sequenceChannel,
//                Data = bytes,
//            };

//            if (_queueMessages.Count < _windowSize)
//            {
//                _queueMessages.Enqueue(packet);
//                return;
//            }
            
//            LoggingManager.GetLogger().LogCritical("[SENT] FULL BUFFER");
//        }
//        private void Request(int sequenceMessage)
//        {
//            lock (_storeMessage)
//            {
//                if (!_storeMessage.TryGetValue(sequenceMessage, out var packet)) return;
                
//                packet.lossPacket++;
//                if (packet.lossPacket % 3 == 0)
//                {
//                    LoggingManager.GetLogger().LogTrace($"[ACK] REQUEST #{sequenceMessage} ({packet.lossPacket} / 3)");

//                    packet.lossPacket = 0;
//                    _storeMessage[sequenceMessage] = packet;

//                    Resend(sequenceMessage);
//                    return;
//                }

//                _storeMessage[sequenceMessage] = packet;
//                LoggingManager.GetLogger().LogTrace($"[ACK] REQUEST #{sequenceMessage} ({packet.lossPacket} / 3)");
//            }
//        }
//        private void Resend(int sequenceMessage)
//        {
//            lock (_storeMessage)
//            {
//                if (_storeMessage.TryGetValue(sequenceMessage, out var packet))
//                {
//                    packet.retryCount++;
//                    packet.sentTime = DateTime.Now;

//                    LoggingManager.GetLogger().LogTrace($"[MSG] RESEND {sequenceMessage}");
//                    _lastSentTime = DateTime.Now;
//                    _networkPointBase.Connection.SendPacket(packet.msg.Pack(), _networkPointBase.ClientEndPoint);
//                    return;
//                }
//            }

//            LoggingManager.GetLogger().LogTrace($"[ACK] DROPPED {sequenceMessage}");
//        }
//        private void OnReceiveAck(int sequenceMessage)
//        {
//            lock (_storeMessage)
//            {
//                if (_storeMessage.ContainsKey(sequenceMessage))
//                {
//                    LoggingManager.GetLogger().LogTrace($"[ACK] RECEIVED #{sequenceMessage}");
//                    _storeMessage.Remove(sequenceMessage);
//                    return;
//                }

//                LoggingManager.GetLogger().LogTrace($"[ACK] OUTDATED #{sequenceMessage}");
//            }
//        }
//        private ushort NextSendSequence()
//        {
//            return (ushort)(_sequenceSended++ % _windowSize);
//        }
//        private void SendFreeMessages()
//        {
//            var maxPackets = _windowSize - _storeMessage.Count;

//            lock (_storeMessage)
//            {
//                while (maxPackets != 0 && _queueMessages.TryDequeue(out var packet))
//                {
//                    packet.Sequence = NextSendSequence();

//                    var message = new NetworkPacketInfo
//                    {
//                        sequence = packet.Sequence,
//                        msg = packet,
//                        sentTime = DateTime.Now
//                    };
                    
//                    _storeMessage.Add(packet.Sequence, message);
//                    _lastSentTime = DateTime.Now;
//                    _networkPointBase.Connection.SendPacket(packet.Pack(), _networkPointBase.ClientEndPoint);

//                    maxPackets--;
//                }
//            }
//        }
//        private void TailLossProbe()
//        {
//            var timeOffset = (DateTime.Now - _lastSentTime).TotalSeconds;
//            if (!(timeOffset > 5)) return;
            
//            foreach (var (sequence, _) in _storeMessage)
//            {
//                LoggingManager.GetLogger().LogTrace("[TEST] SEND LOST PACKETS");
//                Resend(sequence);
//            }
//        }
//        private void RetransmitTimeOut()
//        {
//            var timeNow = DateTime.Now;
//            foreach (var (sequence, packet) in _storeMessage)
//            {
//                var timeout = (timeNow - packet.sentTime).TotalMilliseconds;
//                if(timeout < 30000) continue;

//                LoggingManager.GetLogger().LogDebug($"[ACK] TIMEOUT #{sequence}");
//                if (packet.retryCount > RETRY_COUNT)
//                {
//                    LoggingManager.GetLogger().LogDebug("[ACK] TOO MANY RETRIES");
//                    _storeMessage.Remove(sequence);
//                }

//                LoggingManager.GetLogger().LogDebug($"[ACK] RESEND #{sequence}");
//                Resend(sequence);
//            }
//        }

//        // =====================
//        // ------ Recived ------
//        // =====================

//        private int _sequenceRecived;
//        private Dictionary<int, INetworkMessage> _recivedMessages;

//        private void SendAck(ushort sequence)
//        {
//            var packet = new NetworkReliableOrderedMessage
//            {
//                PacketType = NetworkPacketType.Ack,
//                ChannelId = CHANNEL_ID,
//                Sequence = sequence
//            };
//            _networkPointBase.Connection.SendPacket(packet.Pack(), _networkPointBase.ClientEndPoint);
//        }
//        private void SendRequest(NetworkMessagePacket packet)
//        {
//            packet = new NetworkMessagePacket
//            {
//                PacketType = NetworkPacketType.Request,
//                ChannelId = packet.ChannelId,
//                Sequence = packet.Sequence
//            };
//            _networkPointBase.SendMessage(packet);
//        }
//        private int NextRecivedSequence()
//        {
//            _recivedMessages.Remove(_sequenceRecived++);
//            return _sequenceRecived %= _windowSize;
//        }
//        private void FastRetransmit(NetworkMessagePacket msg)
//        {
//            var sequenceRecived = msg.Sequence;
//            var sequence = sequenceRecived - _sequenceRecived;
//            if (sequence == 0)
//            {
//                LoggingManager.GetLogger().LogInformation($"[MSG] RECEIVED #{sequenceRecived}");
//                ReleaseMessage(msg.Data, msg.ChannelId);

//                var nextSequence = NextRecivedSequence();
//                while (_recivedMessages.ContainsKey(nextSequence))
//                {
//                    LoggingManager.GetLogger().LogInformation($"[MSG] RELEASE #{nextSequence}");
//                    nextSequence = NextRecivedSequence();
//                    ReleaseMessage(msg.Data, msg.ChannelId);
//                }
//                return;
//            }

//            if (sequence < 0)
//            {
//                LoggingManager.GetLogger().LogDebug($"[MSG] DUPLICATE #{sequenceRecived} Delta {sequence}");
//                return;
//            }
            
//            if (sequence > _windowSize)
//            {
//                LoggingManager.GetLogger().LogDebug($"[MSG] TOO EARLY #{sequenceRecived} Expected {_windowSize}");
//                return;
//            }

//            if (_recivedMessages.ContainsKey(sequenceRecived))
//            {
//                LoggingManager.GetLogger().LogDebug($"[MSG] DUPLICATE #{sequenceRecived}");
//                return;
//            }

//            LoggingManager.GetLogger().LogDebug($"[MSG] WAIT #{_sequenceRecived}");
//            _recivedMessages.Add(sequenceRecived, msg);
//            SendRequest(msg);
//        }
        
//        private void ReleaseMessage(byte[] bytes, byte channelId)
//        {
//            _networkPointBase.OnRecivedData(bytes, channelId);
//        }

//        // ====================
//        // ------ Common ------
//        // ====================

//        public void OnRecivePacket(INetworkMessage packet)
//        {
//            NetworkReliableOrderedMessage msg = packet as NetworkReliableOrderedMessage;
//            switch (msg.PacketType)
//            {
//                // Отправитель:
//                case NetworkPacketType.Ack:
//                    OnReceiveAck(msg.Sequence);
//                    break;
                
//                // Отправитель:
//                case NetworkPacketType.Request:
//                    Request(msg.Sequence);
//                    break;
                    
//                // Получатель:
//                case NetworkPacketType.Data:
//                    FastRetransmit(msg);
//                    SendAck(msg.Sequence);
//                    break;
//            }
//        }

//        public override Task OnUpdateAsync()
//        {
//            SendFreeMessages();
//            RetransmitTimeOut();
//            TailLossProbe();
//            return Delay();
//        }

//        public INetworkChannel Clone() => new NetworkReliableOrderedChannel(_networkPointBase);
//    }
//}