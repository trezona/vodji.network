﻿// using System;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Vodji.Network.libNet.Messages;
// using Vodji.Network.Messages;
// using Vodji.Shared;
// using Vodji.Shared.Logging;
//
//
// Добавить таймАут на входящее сообщение
// Переполнение буффера в ожидании
//
// namespace Vodji.Network.libNet.Channels
// {
//     public class NetworkReliableUnorderedChannel : ApplicationCore, INetworkChannel
//     {
//         private NetworkPoint _networkPoint;
//         private int _windowSize;
//         
//         public NetworkReliableUnorderedChannel(NetworkPoint networkPoint)
//         {
//             // Setup
//             _networkPoint = networkPoint;
//             _windowSize = _networkPoint.NetworkConnection.Configuration.WindowSize;
//             
//             // Sender
//             _queueMessages = new Queue<NetworkMessagePacket>();
//             _storeMessage = new Dictionary<int, NetworkPacketInfo>();
//             _lastSentTime = DateTime.Now;
//             
//             // Recived
//             _recivedMessages = new List<int>();
//             
//             // Common
//             StartUpdateAsync();
//         }
//
//         
//         // ====================
//         // ------ Sender ------
//         // ====================
//
//         private const int TIMEOUT = 100;
//         public const int RETRY_COUNT = 40;
//         private Queue<NetworkMessagePacket> _queueMessages;
//         private Dictionary<int, NetworkPacketInfo> _storeMessage;
//         private int _sequenceSended;
//         private DateTime _lastSentTime;
//
//         public void Enqueue(NetworkMessagePacket msg, int sequenceChannel = 0)
//         {
//             if (_queueMessages.Count < _windowSize)
//             {
//                 _queueMessages.Enqueue(msg);
//                 return;
//             }
//             
//             LogManager.GetLogger().LogCritical("[SENT] FULL BUFFER");
//         }
//         private void Request(int sequenceMessage)
//         {
//             lock (_storeMessage)
//             {
//                 if (!_storeMessage.TryGetValue(sequenceMessage, out var packet)) return;
//                 
//                 packet.lossPacket++;
//                 if (packet.lossPacket % 3 == 0)
//                 {
//                     LogManager.GetLogger().LogTrace($"[ASK] REQUEST #{sequenceMessage} ({packet.lossPacket} / 3)");
//
//                     packet.lossPacket = 0;
//                     _storeMessage[sequenceMessage] = packet;
//
//                     Resend(sequenceMessage);
//                     return;
//                 }
//
//                 _storeMessage[sequenceMessage] = packet;
//                 LogManager.GetLogger().LogTrace($"[ASK] REQUEST #{sequenceMessage} ({packet.lossPacket} / 3)");
//             }
//         }
//         private void Resend(int sequenceMessage)
//         {
//             lock (_storeMessage)
//             {
//                 if (_storeMessage.TryGetValue(sequenceMessage, out var packet))
//                 {
//                     packet.retryCount++;
//                     packet.sentTime = DateTime.Now;
//
//                     _lastSentTime = DateTime.Now;
//                     _networkPoint.SendMessage(packet.msg);
//                     return;
//                 }
//             }
//
//             LogManager.GetLogger().LogTrace($"[ASK] DROPPED {sequenceMessage}");
//         }
//         private void OnReceiveAck(int sequenceMessage)
//         {
//             if (_storeMessage.ContainsKey(sequenceMessage))
//             {
//                 LogManager.GetLogger().LogTrace($"[ASK] RECEIVED #{sequenceMessage}");
//                 _storeMessage.Remove(sequenceMessage);
//                 return;
//             }
//             
//             LogManager.GetLogger().LogTrace($"[ASK] OUTDATED #{sequenceMessage}");
//         }
//         private int NextSendSequence()
//         {
//             return _sequenceSended++ % _windowSize;
//         }
//         private void SendFreeMessages()
//         {
//             var maxPackets = _windowSize - _storeMessage.Count;
//
//             lock (_storeMessage)
//             {
//                 while (maxPackets != 0 && _queueMessages.TryDequeue(out var packet))
//                 {
//                     var sequence = NextSendSequence();
//                     packet.Sequence = (ushort)sequence;
//
//                     var message = new NetworkPacketInfo
//                     {
//                         sequence = sequence,
//                         msg = packet,
//                         sentTime = DateTime.Now
//                     };
//                     _storeMessage.Add(sequence, message);
//
//                     _lastSentTime = DateTime.Now;
//                     _networkPoint.SendMessage(packet);
//                     maxPackets--;
//                 }
//             }
//         }
//         private void TailLossProbe()
//         {
//             var timeOffset = (DateTime.Now - _lastSentTime).TotalSeconds;
//             if (!(timeOffset > 5)) return;
//             
//             foreach (var (sequence, _) in _storeMessage)
//             {
//                 LogManager.GetLogger().LogTrace("[TEST] SEND LOST PACKETS");
//                 Resend(sequence);
//             }
//         }
//         private void RetransmitTimeOut()
//         {
//             var timeNow = DateTime.Now;
//             foreach (var (sequence, packet) in _storeMessage)
//             {
//                 var timeout = (timeNow - packet.sentTime).TotalMilliseconds;
//                 if(timeout < 300) continue;
//                 
//                 // LogManager.GetLogger().LogDebug($"[ASK] TIMEOUT #{sequence}");
//                 if (packet.retryCount > RETRY_COUNT)
//                 {
//                     // LogManager.GetLogger().LogDebug("[ASK] TOO MANY RETRIES");
//                     _storeMessage.Remove(sequence);
//                 }
//                 
//                 // LogManager.GetLogger().LogDebug($"[ASK] RESEND #{sequence}");
//                 Resend(sequence);
//             }
//         }
//
//         // =====================
//         // ------ Recived ------
//         // =====================
//
//         private int _sequenceRecived;
//         private List<int> _recivedMessages;
//
//         private void SendAsk(int sequenceRecived)
//         {
//             _networkPoint.SendMessage(new NetworkChannelMessage(sequenceRecived));
//         }
//         private void SendRequest(int sequenceRecived)
//         {
//             _networkPoint.SendMessage(new NetworkChannelMessage(sequenceRecived, true));
//         }
//         private int NextRecivedSequence()
//         {
//             _recivedMessages.Remove(_sequenceRecived++);
//             return _sequenceRecived %= _windowSize;
//         }
//         private void FastRetransmit(NetworkMessagePacket msg)
//         {
//             var sequenceRecived = msg.Sequence;
//             var sequence = sequenceRecived - _sequenceRecived;
//             if (sequence == 0)
//             {
//                 LogManager.GetLogger().LogInformation($"[MSG] RECEIVED #{sequenceRecived}");
//                 ReleaseMessage(msg.Data, msg.ChannelId);
//
//                 var nextSequence = NextRecivedSequence();
//                 while (_recivedMessages.Contains(nextSequence))
//                 {
//                     LogManager.GetLogger().LogInformation($"[MSG] RELEASE #{nextSequence}");
//                     nextSequence = NextRecivedSequence();
//                     ReleaseMessage(msg.Data, msg.ChannelId);
//                 }
//                 return;
//             }
//
//             if (sequence < 0)
//             {
//                 // LogManager.GetLogger().LogDebug($"[MSG] DUPLICATE #{sequenceRecived} Delta {sequence}");
//                 return;
//             }
//             
//             if (sequence > _windowSize)
//             {
//                 // LogManager.GetLogger().LogDebug($"[MSG] TOO EARLY #{sequenceRecived} Expected {_windowSize}");
//                 return;
//             }
//
//             if (_recivedMessages.Contains(sequenceRecived))
//             {
//                 // LogManager.GetLogger().LogDebug($"[MSG] DUPLICATE #{sequenceRecived}");
//                 return;
//             }
//
//             LogManager.GetLogger().LogDebug($"[MSG] WAIT #{_sequenceRecived}");
//             _recivedMessages.Add(sequenceRecived);
//             SendRequest(_sequenceRecived);
//         }
//
//         private void ReleaseMessage(byte[] bytes, byte channelId)
//         {
//             _networkPoint.OnReceivedReadyData(bytes, channelId);
//         }
//         
//         // ====================
//         // ------ Common ------
//         // ====================
//         
//         public void OnRecivePacket(NetworkMessagePacket msg, int sequenceChannel = 0)
//         {
//             switch (msg.PacketType)
//             {
//                 // Отправитель:
//                 case NetworkPacketType.Ack:
//                     OnReceiveAck(msg.Sequence);
//                     break;
//                 
//                 // Отправитель:
//                 case NetworkPacketType.Request: 
//                     Request(msg.Sequence);
//                     break;
//                     
//                 // Получатель:
//                 case NetworkPacketType.Data:
//                     SendAsk(msg.Sequence);
//                     FastRetransmit(msg);
//                     break;
//             }
//         }
//
//         protected override Task OnUpdate()
//         {
//             SendFreeMessages();
//             RetransmitTimeOut();
//             TailLossProbe();
//             return Task.Delay(10);
//         }
//
//         public INetworkChannel Clone()
//         {
//             throw new NotImplementedException();
//         }
//     }
// }