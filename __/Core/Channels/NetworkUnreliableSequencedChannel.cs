﻿//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Vodji.Network.Messages;
//using Vodji.Shared;
//using Vodji.Shared.Logging;

//namespace Vodji.Network.libNet.Channels
//{
//    public class NetworkUnreliableSequencedChannel : ApplicationCore, INetworkChannel
//    {
//        private NetworkPoint _networkPoint;
//        private int _windowSize;
        
//        public NetworkUnreliableSequencedChannel(NetworkPoint networkPoint)
//        {
//            // Setup
//            _networkPoint = networkPoint;
//            _windowSize = 256;//_networkPoint.NetworkConnection.Configuration.WindowSize;
            
//            // Sender
//            _queueMessages = new Queue<NetworkMessagePacket>();
            
//            // Recived
//            StartUpdateAsync();
//        }

        
//        // ====================
//        // ------ Sender ------
//        // ====================

//        private Queue<NetworkMessagePacket> _queueMessages;
//        private int _sequenceSended;

//        public void SendMessage(byte[] bytes, int sequenceChannel = 0)
//        {
//            if (_queueMessages.Count < _windowSize)
//            {
//                _queueMessages.Enqueue(msg);
//                return;
//            }
            
//            LogManager.GetLogger().LogCritical("[SENT] FULL BUFFER");
//        }

//        private int NextSendSequence()
//        {
//            return _sequenceSended++ % _windowSize;
//        }

//        private void SendFreeMessages()
//        {
//            while (_queueMessages.TryDequeue(out var packet))
//            {
//                var sequence = NextSendSequence();
//                packet.Sequence = (ushort)sequence;
//                _networkPoint.SendMessage(packet);
//            }
//        }

//        // =====================
//        // ------ Recived ------
//        // =====================

//        private int _sequenceRecived;
//        private void NextRecivedSequence()
//        {
//            _sequenceRecived = (_sequenceRecived + 1) % _windowSize;
//        }

//        private void ReleaseMessage(byte[] bytes, byte channelId)
//        {
//            _networkPoint.OnReceivedReadyData(bytes, channelId);
//        }
        
//        // ====================
//        // ------ Common ------
//        // ====================
        
//        public void OnRecivePacket(NetworkMessagePacket msg, int sequenceChannel = 0)
//        {
//            var sequenceRecived = msg.Sequence;
//            var sequence = sequenceRecived - _sequenceRecived;

//            if (sequence < 0)
//            {
//                // LogManager.GetLogger().LogDebug($"[MSG] DUPLICATE #{sequenceRecived} Delta {sequence}");
//                return;
//            }

//            LogManager.GetLogger().LogInformation($"[MSG] RECEIVED #{sequenceRecived}");
//            NextRecivedSequence();
//            ReleaseMessage(msg.Data, msg.ChannelId);
//        }

//        protected override Task OnUpdate()
//        {
//            SendFreeMessages();
//            return Task.Delay(10);
//        }

//        public INetworkChannel Clone() => new NetworkUnreliableSequencedChannel(_networkPoint);
//    }
//}