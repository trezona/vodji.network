﻿//using System;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Channels.Interfaces;
//using Vodji.Network.Core;
//using Vodji.Network.Core.Interfaces;
//using Vodji.Network.Identity.Interfaces;
//using Vodji.Network.Utils;

//namespace Vodji.Network.Identity
//{
//    public class NetworkIdentity : INetworkIdentity
//    {
//        public INetworkPoint NetworkPoint => networkPoint;
//        public INetworkChannelHandler ChannelHandler { get; set; }
//        public INetworkHandler NetworkHandler { get; set; }

//        public NetworkIdentity(INetworkPoint networkPoint) => this.networkPoint = networkPoint;

//        public void OnInMessage(byte[] recivedBytes)
//        {
//            ChannelHandler.OnRecivedChannel(recivedBytes);
//            /*if (recivedBytes.Length < NetworkHelper.HeaderMessageSize)
//            {
//                LoggingManager.GetLogger().LogWarning($"Identity {networkPoint} Message was too short (messages should start with message id)");
//                Disconnect();
//                return;
//            }

//            if (NetworkHandler.Invoke(recivedBytes, this))
//            {
//                // code here
//            }*/
//        }


//        public virtual void Disconnect() { }

//        public void SendMessage<T>(ArraySegment<byte> arraySegment) where T : INetworkChannel
//        {
//            LoggingManager.GetLogger().LogTrace($"Send to {NetworkPoint} bytes:" + BitConverter.ToString(arraySegment.Array, arraySegment.Offset, arraySegment.Count));
//            ChannelHandler.OnSendingChannel(arraySegment.Array, NetworkHelper.GetNetworkId<T>());
//        }

//        protected INetworkPoint networkPoint;
//    }
//}
