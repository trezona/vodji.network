﻿//using System;
//using System.Reflection;
//using Vodji.Shared.Extensions;

//namespace Vodji.Network.Interfaces
//{
//    public interface INetworkMessage
//    {
//        public void PackMessage(INetworkWriter networkWriter)
//        {
//            var messageId = GetType().GetStableShortCode();
//            networkWriter.Write(messageId);
//            networkWriter.Write(this);
//        }


//        public void Serialize(INetworkWriter networkWriter)
//        {
//            foreach (var item in GetType().GetProperties())
//            {
//                if (item.GetCustomAttribute<NonSerializedAttribute>() is not null) continue;
//                networkWriter.Write(item.GetValue(this));
//            }
//        }

//        public virtual void Deserialize(INetworkReader networkReader)
//        {
//            foreach (var item in GetType().GetProperties())
//            {
//                if (item.GetCustomAttribute<NonSerializedAttribute>() is not null) continue;
//                item.SetValue(this, networkReader.Read(item.PropertyType));
//            }
//        }
//    }
//}
