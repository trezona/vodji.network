﻿using System;
using System.Text;

namespace Vodji.Network.v2
{
    internal enum MessageResendReason
    {
        Delay,
        HoleInSequence
    }

    public class NetworkStatistics
    {
        private int m_sentPackets;
        private int m_receivedPackets;

        private int m_sentMessages;
        private int m_receivedMessages;
        private int m_receivedFragments;
        private int m_droppedMessages;

        private int m_sentBytes;
        private int m_receivedBytes;

        private int m_resentMessagesDueToDelay;
        private int m_resentMessagesDueToHole;

        private DateTime m_lastTimeOffset = DateTime.UtcNow;

        public void Reset()
        {
            m_sentPackets = 0;
            m_receivedPackets = 0;

            m_sentMessages = 0;
            m_receivedMessages = 0;
            m_receivedFragments = 0;

            m_sentBytes = 0;
            m_receivedBytes = 0;

            m_resentMessagesDueToDelay = 0;
            m_resentMessagesDueToHole = 0;
        }

        /// <summary>
        /// Gets the last time of recived packets or since the Network was initialized
        /// </summary>
        public int LastTimeOffset => (DateTime.UtcNow - m_lastTimeOffset).Seconds;

        /// <summary>
        /// Gets the number of sent packets since the Network was initialized
        /// </summary>
        public int SentPackets
        {
            get => m_sentPackets;
            set => m_sentPackets += value;
        }

        /// <summary>
        /// Gets the number of received packets since the Network was initialized
        /// </summary>
        public int ReceivedPackets
        {
            get => m_receivedPackets;
            set
            {
                m_lastTimeOffset = DateTime.UtcNow;
                m_receivedPackets += value;
            }
        }

        /// <summary>
        /// Gets the number of sent messages since the Network was initialized
        /// </summary>
        public int SentMessages
        {
            get => m_sentMessages;
            set => m_sentMessages += value;
        }

        /// <summary>
        /// Gets the number of received messages since the Network was initialized
        /// </summary>
        public int ReceivedMessages
        {
            get => m_receivedMessages;
            set => m_receivedMessages += value;
        }

        /// <summary>
		/// Gets the number of resent reliable messages for this connection
		/// </summary>
		public long ResentMessages => m_resentMessagesDueToHole + m_resentMessagesDueToDelay;

        /// <summary>
        /// Gets the number of dropped messages for this connection
        /// </summary>
        public int DroppedMessages
        {
            get => m_droppedMessages;
            set => m_droppedMessages += value;
        }

        /// <summary>
        /// Gets the number of sent bytes since the Network was initialized
        /// </summary>
        public int SentBytes
        {
            get => m_sentBytes;
            set => m_sentBytes += value;
        }

        /// <summary>
        /// Gets the number of received bytes since the Network was initialized
        /// </summary>
        public int ReceivedBytes
        {
            get => m_receivedBytes;
            set => m_receivedBytes += value;
        }

        private void MessageResent(MessageResendReason reason)
        {
            if (reason == MessageResendReason.Delay) m_resentMessagesDueToDelay++;
            else m_resentMessagesDueToHole++;
        }

        private void MessageDropped() => m_droppedMessages++;

        /// <summary>
        /// Returns a string that represents this object
        /// </summary>
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            //stringBuilder.AppendLine("Current MTU: " + m_connection.m_currentMTU");
            stringBuilder.AppendLine("Sent " + m_sentBytes + " bytes in " + m_sentMessages + " messages in " + m_sentPackets + " packets");
            stringBuilder.AppendLine("Received " + m_receivedBytes + " bytes in " + m_receivedMessages + " messages (of which " + m_receivedFragments + " fragments) in " + m_receivedPackets + " packets");
            stringBuilder.AppendLine("Dropped " + m_droppedMessages + " messages (dupes/late/early)");
            if (m_resentMessagesDueToDelay > 0) stringBuilder.AppendLine("Resent messages (delay): " + m_resentMessagesDueToDelay);
            if (m_resentMessagesDueToHole > 0) stringBuilder.AppendLine("Resent messages (holes): " + m_resentMessagesDueToHole);

            int numUnsent = 0;
            int numStored = 0;
            //foreach (NetSenderChannelBase sendChan in m_connection.m_sendChannels)
            //{
            //    if (sendChan == null)
            //        continue;
            //    numUnsent += sendChan.QueuedSendsCount;

            //    var relSendChan = sendChan as NetReliableSenderChannel;
            //    if (relSendChan != null)
            //    {
            //        for (int i = 0; i < relSendChan.m_storedMessages.Length; i++)
            //            if (relSendChan.m_storedMessages[i].Message != null)
            //                numStored++;
            //    }
            //}

            int numWithheld = 0;
            //foreach (NetReceiverChannelBase recChan in m_connection.m_receiveChannels)
            //{
            //    var relRecChan = recChan as NetReliableOrderedReceiver;
            //    if (relRecChan != null)
            //    {
            //        for (int i = 0; i < relRecChan.m_withheldMessages.Length; i++)
            //            if (relRecChan.m_withheldMessages[i] != null)
            //                numWithheld++;
            //    }
            //}

            stringBuilder.AppendLine("Unsent messages: " + numUnsent);
            stringBuilder.AppendLine("Stored messages: " + numStored);
            stringBuilder.AppendLine("Withheld messages: " + numWithheld);

            //stringBuilder.AppendLine(m_peer.ConnectionsCount.ToString() + " connections");
            //stringBuilder.AppendLine("Storage allocated " + m_bytesAllocated + " bytes");
            //stringBuilder.AppendLine("Recycled pool " + m_peer.m_storagePoolBytes + " bytes (" + m_peer.m_storageSlotsUsedCount + " entries)");
            return stringBuilder.ToString();
        }
    }
}
