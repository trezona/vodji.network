﻿//using System;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Data;
//using Vodji.Network.Data.Interface;
//using Vodji.Network.Utils;
//using Vodji.Network.v2.Components;
//using Vodji.Network.v2.Connections.Interfaces;

//namespace Vodji.Network.v2.Common
//{
//    public class NetworkIdentity
//    {
//        public INetworkPoint NetworkPoint { get; }

//        public NetworkIdentity(INetworkPoint networkPoint) => NetworkPoint = networkPoint;

//        public void Disconnect() => NetworkPoint.Disconnect();

//        public async void DatagramReceived(byte[] bytesRecived)
//        {
//            using (INetworkReader networkReader = new NetworkReader(bytesRecived))
//            {
//                await m_networkMiddleware.InvokeReaderAsync(networkReader);
//                if (m_networkHandler.Invoke(networkReader, this))
//                {
//                    NetworkPoint.NetworkStatistics.ReceivedMessages++;
//                    return;
//                }

//                LoggingManager.GetLogger().LogTrace($"Closed connection: {NetworkPoint}. Invalid message header.");
//                Disconnect();
//            }
//        }

//        public async void DatagramSending<IMessage>(IMessage networkMessage) where IMessage : INetworkMessage
//        {
//            using (var networkWriter = new NetworkWriter())
//            {
//                await m_networkMiddleware.InvokeWriterAsync(networkWriter, networkMessage);

//                networkWriter.Write(NetworkHelper.GetNetworkId(networkMessage.GetType()));
//                networkMessage.Serialize(networkWriter);

//                LoggingManager.GetLogger().LogTrace($"Message {typeof(IMessage).Name} sent to {NetworkPoint}");
//                NetworkPoint.SendMessage(networkWriter.ToArray());
//            }
//        }


//        public void SetMiddleware(NetworkChannels networkMiddleware) => m_networkMiddleware = networkMiddleware;
//        private NetworkChannels m_networkMiddleware;

//        public void SetHandler(NetworkHandler networkHandler) => m_networkHandler = networkHandler;
//        private NetworkHandler m_networkHandler;
//    }
//}
