﻿//using System;
//using System.Net;
//using System.Net.Sockets;
//using System.Threading;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;

//namespace Vodji.Network.v2.Connections.UDP
//{
//    public abstract class NetworkUDPSocket : NetworkUDPOptions, IDisposable
//    {
//        public async void ListenAsync() => await ListenAsyncTask();
//        public Task ListenAsyncTask() => Task.Factory.StartNew(Listen);
//        public void Listen()
//        {
//            using var mutex = new Mutex(false, "Global\\NetworkSocketBind");
//            try
//            {
//                mutex.WaitOne();
//                if(udpClient is not null)
//                {
//                    LoggingManager.Logger.LogWarning("udpClient is already exist.");
//                    return;
//                }

//                var selfAdress = new IPEndPoint(Address, Port);
//                udpClient = new UdpClient(Address.AddressFamily);
//                {
//                    udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, UseReuseAdress);
//                    udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ExclusiveAddressUse, UseExclusiveAddress);
//                    udpClient.Client.ReceiveBufferSize = ReceiveBufferSize;
//                    udpClient.Client.SendBufferSize = SendBufferSize;
//                    udpClient.Client.Blocking = false;

//                    if (Address.Equals(AddressFamily.InterNetworkV6)) udpClient.Client.DualMode = UseDualMode;
//                    try
//                    {
//                        const uint IOC_IN = 0x80000000;
//                        const uint IOC_VENDOR = 0x18000000;
//                        const uint SIO_UDP_CONNRESET = (IOC_IN | IOC_VENDOR | 12);
//                        udpClient.Client.IOControl(unchecked((int)SIO_UDP_CONNRESET), new[] { Convert.ToByte(false) }, null);
//                    }
//                    catch (Exception e)
//                    {
//                        LoggingManager.GetLogger().LogError(e, "SIO_UDP_CONNRESET not supported on this platform");
//                    }

//                    udpClient.Client.Bind(selfAdress);
//                    udpClient.BeginReceive(ReceiveData, this);
//                }
//            }
//            finally
//            {
//                mutex.ReleaseMutex();
//            }

//            Address = ((IPEndPoint)udpClient.Client.LocalEndPoint).Address;
//            Port = ((IPEndPoint)udpClient.Client.LocalEndPoint).Port;
//            Lock();
            
//            var selfBindingAdress = new IPEndPoint(Address, Port);
//            LoggingManager.GetLogger().LogInformation($"Socket bound to {selfBindingAdress} Listen: {udpClient.Client.IsBound}");
//        }

//        // Recived Data
//        protected abstract void OnRecivedData(IPEndPoint clientPoint, byte[] recivedBytes);
//        private void ReceiveData(IAsyncResult ar)
//        {
//            var clientPoint = new IPEndPoint(IPAddress.Any, 0);
//            var recivedBytes = udpClient.EndReceive(ar, ref clientPoint);

//            LoggingManager.GetLogger().LogTrace($"Received {recivedBytes.Length} bytes from {clientPoint}");
//            OnRecivedData(clientPoint, recivedBytes.Length is 0 ? null : recivedBytes);

//            udpClient.BeginReceive(ReceiveData, this);
//        }

//        // Send Data
//        protected abstract void OnSendData(int sentNumBytes, IPEndPoint clientPoint);
//        private void SendData(IAsyncResult ar)
//        {
//            var (numBytes, clientPoint) = ar.AsyncState as Tuple<int, IPEndPoint>;

//            var sentNumBytes = udpClient.EndSend(ar);
//            if (numBytes != sentNumBytes)
//            {
//                LoggingManager.GetLogger().LogWarning($"Failed to send the full {numBytes} only {sentNumBytes} bytes sent in packet!");
//            }

//            LoggingManager.GetLogger().LogTrace($"Send {sentNumBytes} bytes via UDP to specifically client {clientPoint}.");
//            OnSendData(sentNumBytes, clientPoint);
//        }

//        // Method Send
//        public SocketError SendPacket(byte[] bytes, IPEndPoint clientPoint) => SendPacket(bytes is null ? new byte[0] : bytes, bytes is null ? 0 : bytes.Length, clientPoint);
//        public SocketError SendPacket(byte[] bytes, int numBytes, IPEndPoint clientPoint)
//        {
//            // NetworkStatistics.PacketSent(numBytes, numMessages);
//            try
//            {
//                if (clientPoint.Address.Equals(IPAddress.Broadcast))
//                {
//                    udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
//                }

//                udpClient.BeginSend(bytes, numBytes, clientPoint, SendData, new Tuple<int, IPEndPoint>(numBytes, clientPoint));
//            }
//            catch (SocketException socketException)
//            {
//                switch (socketException.SocketErrorCode)
//                {
//                    case SocketError.WouldBlock: // send buffer full?
//                        LoggingManager.GetLogger().LogWarning("Increase Buffer in NetworkConfiguration");
//                        return SocketError.WouldBlock;

//                    default:
//                        LoggingManager.GetLogger().LogError(socketException, "Failed to send packet: (" + socketException.SocketErrorCode + ") ");
//                        return socketException.SocketErrorCode;
//                }
//            }
//            catch
//            {
//                LoggingManager.GetLogger().LogError("Failed to send packet: ");
//                return SocketError.Fault;
//            }
//            finally
//            {
//                if (clientPoint.Address.Equals(IPAddress.Broadcast))
//                {
//                    udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);
//                }
//            }

//            return SocketError.Success;
//        }

//        public void Dispose()
//        {
//            if(udpClient is null)
//            {
//                LoggingManager.Logger.LogWarning("Vodji Socket is not exist.");
//                return;
//            }

//            try
//            {
//                if (udpClient.Client != null)
//                {

//                    try
//                    {
//                        udpClient.Client.Shutdown(SocketShutdown.Receive);
//                    }
//                    catch (Exception e)
//                    {
//                        LoggingManager.GetLogger().LogError(e, "Socket.Shutdown exception");
//                    }

//                    try
//                    {
//                        udpClient.Client.Close();
//                    }
//                    catch (Exception e)
//                    {
//                        LoggingManager.GetLogger().LogError(e, "UdpClient.Close exception");
//                    }
//                }
//            }
//            finally
//            {
//                udpClient = null;
//                LoggingManager.GetLogger().LogDebug("Shutdown complete");
//            }

//            // m_connections.Clear();
//            // m_connectionLookup.Clear();
//            // m_handshakes.Clear();
//            GC.SuppressFinalize(this);
//        }

//        protected UdpClient udpClient;
//    }
//}
