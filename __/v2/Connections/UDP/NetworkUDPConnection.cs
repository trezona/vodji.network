﻿//using System.Linq;
//using System.Net;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.v2.Common;
//using Vodji.Network.v2.Connections.Interfaces;
//using Vodji.Shared.Utils;

//namespace Vodji.Network.v2.Connections.UDP
//{
//    public class NetworkUDPConnection : NetworkUDPSocket, INetworkConnection
//    {
//        public EventCallback<INetworkPoint> EventClientConnected { get; } = new EventCallback<INetworkPoint>();
//        public EventCallback<INetworkPoint> EventClientDisconnected { get; } = new EventCallback<INetworkPoint>();
//        public EventCallback<INetworkPoint, byte[]> EventRecivedDatagram { get; } = new EventCallback<INetworkPoint, byte[]>();
//        public EventCallback<INetworkPoint, int> EventSendingDatagram { get; } = new EventCallback<INetworkPoint, int>();

//        public void SendMessageToAll(byte[] datagram) => SendMessageToConnections(connections.KeysT2.ToArray(), datagram is null ? new byte[0] : datagram);
//        public void SendMessageToConnection(string connection, byte[] datagram) => SendPacket(datagram, NetworkUtils.Parse(connection));
//        public void SendMessageToConnection(INetworkPoint networkPoint, byte[] datagram) => SendMessageToConnections(new[] { networkPoint }, datagram);
//        public void SendMessageToConnections(INetworkPoint[] networkPoints, byte[] datagram)
//        {
//            foreach (var networkPoint in networkPoints)
//            {
//                if (!connections.TryGetValue(networkPoint, out var endPoint))
//                {
//                    LoggingManager.GetLogger().LogWarning($"{networkPoint} unknown sender. Lets just accept Connect");
//                    continue;
//                }

//                if (networkPoint.NetworkStatistics.LastTimeOffset > Timeout)
//                {
//                    LoggingManager.GetLogger().LogWarning($"Disconnecting {networkPoint} for inactivity!");
//                    Disconnect(networkPoint);
//                    continue;
//                }

//                if (networkPoint.NetworkStatistics.LastTimeOffset > Timeout / 2)
//                    LoggingManager.GetLogger().LogWarning($"{networkPoint} last connection, try restore connection.");

//                LoggingManager.GetLogger().LogWarning($"{datagram?.Length}");
//                SendPacket(datagram, endPoint);
//            }
//        }

//        public void Disconnect(INetworkPoint networkPoint)
//        {
//            if (!connections.Contains(networkPoint))
//            {
//                LoggingManager.GetLogger().LogWarning($"Invalid connection: {networkPoint}");
//                return;
//            }

//            LoggingManager.GetLogger().LogTrace($"Kicked: {networkPoint}");
//            EventClientDisconnected.Invoke(networkPoint);
//            connections.Remove(networkPoint);
//        }

//        protected override void OnRecivedData(IPEndPoint clientPoint, byte[] recivedBytes)
//        {
//            if (!connections.TryGetValue(clientPoint, out var networkPoint))
//            {
//                networkPoint = new NetworkPoint(this);
//                {
//                    connections.Add(networkPoint, clientPoint);
//                    EventClientConnected.Invoke(networkPoint);
//                }

//                if (recivedBytes is null)
//                {
//                    SendMessageToConnection(networkPoint, null);
//                    return;
//                }
//            }
//            if (recivedBytes is null) return;



//            networkPoint.NetworkStatistics.ReceivedBytes = recivedBytes.Length;
//            networkPoint.NetworkStatistics.ReceivedPackets++;

//            EventRecivedDatagram.Invoke(networkPoint, recivedBytes);
//        }

//        protected override void OnSendData(int sentNumBytes, IPEndPoint clientPoint)
//        {
//            if (connections.TryGetValue(clientPoint, out var networkPoint))
//            {
//                networkPoint.NetworkStatistics.SentPackets++;
//                networkPoint.NetworkStatistics.SentBytes = sentNumBytes;
//                EventSendingDatagram.Invoke(networkPoint, sentNumBytes);
//            }
//        }

//        protected DictionaryPair<IPEndPoint, INetworkPoint> connections = new DictionaryPair<IPEndPoint, INetworkPoint>();
//    }
//}
