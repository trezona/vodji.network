﻿//using System;
//using System.Collections.Generic;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Data;
//using Vodji.Network.Data.Interface;
//using Vodji.Network.Utils;
//using Vodji.Network.v2.Common;

//namespace Vodji.Network.v2.Components
//{
//    public delegate void NetworkInvokeDelegate(INetworkReader networkReader, NetworkIdentity networkIdentity);
//    public class NetworkHandler
//    {
//        public bool AddListener<TMessage>(Action<TMessage> funcDelegate) where TMessage : INetworkMessage, new()
//        {
//            return AddListener((TMessage value, NetworkIdentity _) => funcDelegate(value));
//        }
//        public bool AddListener<TMessage>(Action<TMessage, NetworkIdentity> funcDelegate) where TMessage : INetworkMessage, new()
//        {
//            var funcId = NetworkHelper.GetNetworkId<TMessage>();
//            var generatedDelegate = OnGenerateDelegate(funcDelegate);
//            if(listDelegates.TryAdd(funcId, generatedDelegate))
//            {
//                LoggingManager.GetLogger().LogWarning($"Adding handler for {typeof(TMessage).FullName}, id={funcId}.");
//                return true;
//            }

//            LoggingManager.GetLogger().LogWarning($"If replacement is intentional, use ReplaceListener<{typeof(TMessage).FullName}>.");
//            return false;
//        }

//        public bool ReplaceListener<TMessage>(Action<TMessage> funcDelegate) where TMessage : INetworkMessage, new()
//        {
//            return ReplaceListener((TMessage value, NetworkIdentity _) => funcDelegate(value));
//        }
//        public bool ReplaceListener<TMessage>(Action<TMessage, NetworkIdentity> funcDelegate) where TMessage : INetworkMessage, new()
//        {
//            int funcId = NetworkHelper.GetNetworkId<TMessage>();
//            if (listDelegates.TryGetValue(funcId, out _))
//            {
//                listDelegates[funcId] = OnGenerateDelegate(funcDelegate);
//                LoggingManager.GetLogger().LogWarning($"Replacing handler {typeof(TMessage).FullName}, id={funcId}.");
//                return true;
//            }

//            //Обработчик отсуствует, необходимо добавить новый AddListener
//            LoggingManager.GetLogger().LogWarning($"The handler is missing, you need to add a new AddListener<{typeof(TMessage).FullName}>");
//            return false;
//        }

//        public bool RemoveListener(int funcId)
//        {
//            if (listDelegates.Remove(funcId))
//            {
//                LoggingManager.GetLogger().LogWarning($"The {funcId} handler removed.");
//                return true;
//            }

//            LoggingManager.GetLogger().LogWarning($"The {funcId} handler is missing, dont remove.");
//            return false;
//        }
//        public bool RemoveListener<TMessage>() where TMessage : INetworkMessage
//        {
//            int funcId = NetworkHelper.GetNetworkId<TMessage>();
//            if (listDelegates.Remove(funcId))
//            {
//                LoggingManager.GetLogger().LogWarning($"The {typeof(TMessage).FullName} handler removed.");
//                return true;
//            }

//            LoggingManager.GetLogger().LogWarning($"The {typeof(TMessage).FullName} handler is missing, dont remove.");
//            return false;
//        }

//        public bool Invoke(byte[] recivedBytes, NetworkIdentity networkIdentity) => Invoke(new NetworkReader(recivedBytes), networkIdentity);
//        public bool Invoke(INetworkReader networkReader, NetworkIdentity networkIdentity)
//        {
//            if(NetworkHelper.TryUnpackMessage(networkReader, out var funcId))
//            {
//                LoggingManager.GetLogger().LogInformation($"msgType:{funcId} Received: {networkReader.Length} via UDP from {networkIdentity.NetworkPoint}");
//                if(listDelegates.TryGetValue(funcId, out var generatedDelegate))
//                {
//                    generatedDelegate.Invoke(networkReader, networkIdentity);
//                    return true;
//                }

//                LoggingManager.GetLogger().LogWarning($"Unknown message ID {funcId}. May be due to no existing AddListener for this message.");
//                return false;
//            }

//            LoggingManager.GetLogger().LogError($"Closed connection: {networkIdentity.NetworkPoint}. Invalid message header.");
//            return false;
//        }

//        protected virtual NetworkInvokeDelegate OnGenerateDelegate<TMessage>(Action<TMessage, NetworkIdentity> func) where TMessage : INetworkMessage, new()
//        {
//            return (networkReader, networkIdentity) =>
//            {
//                TMessage networkMessage = default;
//                try
//                {
//                    networkMessage ??= new TMessage();
//                    networkMessage.Deserialize(networkReader);
//                }
//                catch (Exception e)
//                {
//                    LoggingManager.GetLogger().LogError($"Closed connection: {networkIdentity.NetworkPoint}. Invalid data. Reason: {e}");
//                    return;
//                }
//                finally 
//                {
//                    func.Invoke(networkMessage, networkIdentity);
//                }
//            };
//        }
//        protected Dictionary<int, NetworkInvokeDelegate> listDelegates = new Dictionary<int, NetworkInvokeDelegate>();
//    }
//}
