﻿//using Vodji.Network.libNet;
//using Vodji.Network.Messages;
//using Vodji.Shared.Logging;

//namespace Vodji.Network
//{
//    public class NetworkServer : NetworkConnection
//    {
//        public bool Initialized { get; private set; }

//        public NetworkServer(NetworkConfiguration config) : base(config)
//        {
//        }

//        public void Initialize()
//        {
//            if(Initialized) return;
            
//            LogManager.GetLogger().LogTrace("Network Server Initialize");
//            Initialized = true;

//            if (Authenticator != null)
//            {
//                Authenticator.OnServerInitialize();
//                Authenticator.OnServerAuthenticated.AddListener(OnServerAuthenticated);
//            }
//        }
        
//        // Вызывается при первом сообщении о подключении
//        protected virtual void OnServerConnect(NetworkPoint networkPoint, ConnectMessage connectMessage)
//        {
//            LogManager.GetLogger().LogTrace("NetworkConnection.OnServerConnect");

//            if (Authenticator != null)
//            {
//                Authenticator.OnServerAuthenticate(networkPoint);
//                return;
//            }

//            OnServerAuthenticated(networkPoint);
//        }
        
//        // Вызывается при отсуствии аунтификации
//        protected virtual void OnServerAuthenticated(NetworkPoint networkPoint)
//        {
//            LogManager.GetLogger().LogTrace("NetworkConnection.OnServerAuthenticated");

//            networkPoint.IsAuthenticated = true;
//            OnServerAccepted(networkPoint);
//        }
        
//        // Вызывается при прохождении аунтификации
//        // Добавляем клиента, в список подключенных
//        protected virtual void OnServerAccepted(NetworkPoint networkPoint)
//        {
//            LogManager.GetLogger().LogTrace($"OnServerAccepted Client: {networkPoint}");

//            if (connections.Contains(networkPoint))
//            {
//                LogManager.GetLogger().LogTrace($"Connection {networkPoint} already in use. Kicked");
//                Disconnect(networkPoint);
//                return;
//            }

//            if (connections.Count >= Configuration.MaximumConnections)
//            {
//                LogManager.GetLogger().LogTrace($"Server full. Kicked {networkPoint}");
//                return;
//            }

//            connections.Add(networkPoint);
//        }

//        public void Disconnect(NetworkPoint networkPoint, DisconnectMessage disconnectMessage = null)
//        {
            
//        }

//        protected void OnServerDisconnect(NetworkPoint networkPoint, DisconnectMessage disconnectMessage)
//        {
//            LogManager.GetLogger().LogTrace($"Client disconnected: {networkPoint}");
//        }
//    }
//}