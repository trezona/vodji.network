﻿//using System;
//using System.Drawing;
//using System.Runtime.CompilerServices;
//using System.Text;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Utils;

//namespace Vodji.Network
//{
//    public class NetworkWriter : INetworkWriter
//    {
//        public void Write(object value)
//        {
//            if (value is byte byteValue) WriteByte(byteValue);
//            else if (value is byte[] bytesValue) WriteBytesAndSize(bytesValue);
//            else if (value is sbyte sbyteValue) WriteByte((byte)sbyteValue);
//            else if (value is ushort uint16Value) WriteUInt16(uint16Value);
//            else if (value is uint uintValue) WriteUInt32(uintValue);
//            else if (value is ulong ulongValue) WriteUInt64(ulongValue);
//            else if (value is int intValue) WriteInt32(intValue);
//            else if (value is long longValue) WriteInt64(longValue);
//            else if (value is bool boolValue) WriteByte((byte)(boolValue ? 1 : 0));
//            else if (value is char charValue) WriteUInt16(charValue);
//            else if (value is short shortValue) WriteUInt16((ushort)shortValue);
//            else if (value is float floatValue) WriteSingle(floatValue);
//            else if (value is double doubleValue) WriteDouble(doubleValue);
//            else if (value is decimal decimalValue) WriteDecimal(decimalValue);
//            else if (value is string stringValue) WriteString(stringValue);
//            else if (value is Uri uriValue) WriteUri(uriValue);
//            else if (value is Guid guidValue) WriteGuid(guidValue);
//            else if (value is Color colorValue) WriteColor(colorValue);
//            else if (value is ArraySegment<byte> arraySegmentValue) WriteBytesAndSizeSegment(arraySegmentValue);
//            else LoggingManager.GetLogger().LogWarning($"Not support {value.GetType()}");
//        }

//        #region Writers
//        protected void WriteByte(byte value)
//        {
//            EnsureLength(_position + 1);
//            _buffer[_position++] = value;
//        }
//        protected void WriteBytes(byte[] buffer, int offset, int count)
//        {
//            EnsureLength(_position + count);
//            Array.ConstrainedCopy(buffer, offset, this._buffer, _position, count);
//            _position += count;
//        }
//        protected void WriteUInt32(uint value)
//        {
//            EnsureLength(_position + 4);
//            _buffer[_position++] = (byte)value;
//            _buffer[_position++] = (byte)(value >> 8);
//            _buffer[_position++] = (byte)(value >> 16);
//            _buffer[_position++] = (byte)(value >> 24);
//        }
//        protected void WriteInt32(int value) => WriteUInt32((uint)value);
//        protected void WriteUInt64(ulong value)
//        {
//            EnsureLength(_position + 8);
//            _buffer[_position++] = (byte)value;
//            _buffer[_position++] = (byte)(value >> 8);
//            _buffer[_position++] = (byte)(value >> 16);
//            _buffer[_position++] = (byte)(value >> 24);
//            _buffer[_position++] = (byte)(value >> 32);
//            _buffer[_position++] = (byte)(value >> 40);
//            _buffer[_position++] = (byte)(value >> 48);
//            _buffer[_position++] = (byte)(value >> 56);
//        }
//        protected void WriteInt64(long value) => WriteUInt64((ulong)value);
//        protected void WriteUInt16(ushort value)
//        {
//            WriteByte((byte)value);
//            WriteByte((byte)(value >> 8));
//        }
//        protected void WriteSingle(float value)
//        {
//            UIntFloat converter = new UIntFloat
//            {
//                floatValue = value
//            };
//            WriteUInt32(converter.intValue);
//        }
//        protected void WriteDouble(double value)
//        {
//            UIntDouble converter = new UIntDouble
//            {
//                doubleValue = value
//            };
//            WriteUInt64(converter.longValue);
//        }
//        protected void WriteDecimal(decimal value)
//        {
//            UIntDecimal converter = new UIntDecimal
//            {
//                decimalValue = value
//            };
//            WriteUInt64(converter.longValue1);
//            WriteUInt64(converter.longValue2);
//        }
//        protected void WriteString(string value)
//        {
//            // write 0 for null support, increment real size by 1
//            // (note: original HLAPI would write "" for null strings, but if a
//            //        string is null on the server then it should also be null
//            //        on the client)
//            if (value == null)
//            {
//                WriteUInt16(0);
//                return;
//            }

//            // write string with same method as NetworkReader
//            // convert to byte[]
//            int size = _encoding.GetBytes(value, 0, value.Length, _stringBuffer, 0);

//            // check if within max size
//            if (size >= 1024 * 32)
//            {
//                LoggingManager.GetLogger().LogError("String too long: " + size + ". Limit: " + 1024 * 32);
//                return;
//            }

//            // write size and bytes
//            WriteUInt16(checked((ushort)(size + 1)));
//            WriteBytes(_stringBuffer, 0, size);
//        }
//        protected void WriteBytesAndSize(byte[] buffer, int offset, int count)
//        {
//            // null is supported because [SyncVar]s might be structs with null byte[] arrays
//            // write 0 for null array, increment normal size by 1 to save bandwith
//            // (using size=-1 for null would limit max size to 32kb instead of 64kb)
//            if (buffer == null)
//            {
//                WritePackedUInt32(0u);
//                return;
//            }
//            WritePackedUInt32(checked((uint)count) + 1u);
//            WriteBytes(buffer, offset, count);
//        }
//        protected void WriteBytesAndSize(byte[] buffer)
//        {
//            WriteBytesAndSize(buffer, 0, buffer != null ? buffer.Length : 0);
//        }
//        protected void WriteBytesAndSizeSegment(ArraySegment<byte> buffer)
//        {
//            WriteBytesAndSize(buffer.Array, buffer.Offset, buffer.Count);
//        }
//        protected void WritePackedInt32(int i)
//        {
//            // zigzag encoding https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
//            uint zigzagged = (uint)((i >> 31) ^ (i << 1));
//            WritePackedUInt32(zigzagged);
//        }
//        protected void WritePackedUInt32(uint value)
//        {
//            // http://sqlite.org/src4/doc/trunk/www/varint.wiki
//            // for 32 bit values WritePackedUInt64 writes the
//            // same exact thing bit by bit
//            WritePackedUInt64(value);
//        }
//        protected void WritePackedInt64(long i)
//        {
//            // zigzag encoding https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
//            ulong zigzagged = (ulong)((i >> 63) ^ (i << 1));
//            WritePackedUInt64(zigzagged);
//        }
//        protected void WritePackedUInt64(ulong value)
//        {
//            if (value <= 240)
//            {
//                WriteByte((byte)value);
//                return;
//            }
//            if (value <= 2287)
//            {
//                WriteByte((byte)(((value - 240) >> 8) + 241));
//                WriteByte((byte)(value - 240));
//                return;
//            }
//            if (value <= 67823)
//            {
//                WriteByte(249);
//                WriteByte((byte)((value - 2288) >> 8));
//                WriteByte((byte)(value - 2288));
//                return;
//            }
//            if (value <= 16777215)
//            {
//                WriteByte(250);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                return;
//            }
//            if (value <= 4294967295)
//            {
//                WriteByte(251);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                WriteByte((byte)(value >> 24));
//                return;
//            }
//            if (value <= 1099511627775)
//            {
//                WriteByte(252);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                WriteByte((byte)(value >> 24));
//                WriteByte((byte)(value >> 32));
//                return;
//            }
//            if (value <= 281474976710655)
//            {
//                WriteByte(253);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                WriteByte((byte)(value >> 24));
//                WriteByte((byte)(value >> 32));
//                WriteByte((byte)(value >> 40));
//                return;
//            }
//            if (value <= 72057594037927935)
//            {
//                WriteByte(254);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                WriteByte((byte)(value >> 24));
//                WriteByte((byte)(value >> 32));
//                WriteByte((byte)(value >> 40));
//                WriteByte((byte)(value >> 48));
//                return;
//            }

//            // all others
//            {
//                WriteByte(255);
//                WriteByte((byte)value);
//                WriteByte((byte)(value >> 8));
//                WriteByte((byte)(value >> 16));
//                WriteByte((byte)(value >> 24));
//                WriteByte((byte)(value >> 32));
//                WriteByte((byte)(value >> 40));
//                WriteByte((byte)(value >> 48));
//                WriteByte((byte)(value >> 56));
//            }
//        }
//        protected void WriteColor(Color value)
//        {
//            WriteSingle(value.R);
//            WriteSingle(value.G);
//            WriteSingle(value.B);
//            WriteSingle(value.A);
//        }
//        protected void WriteGuid(Guid value)
//        {
//            byte[] data = value.ToByteArray();
//            WriteBytes(data, 0, data.Length);
//        }
//        protected void WriteUri(Uri uri)
//        {
//            WriteString(uri.ToString());
//        }
//        #endregion

//        public void WriteMessage<T>(T msg) where T : INetworkMessage => msg.Serialize(this);

//        public int Length => _length;
//        public int Position
//        {
//            get => _position;
//            set
//            {
//                _position = value;
//                EnsureLength(value);
//            }
//        }
//        public byte[] ToArray()
//        {
//            byte[] data = new byte[_length];
//            Array.ConstrainedCopy(_buffer, 0, data, 0, _length);
//            return data;
//        }
//        public ArraySegment<byte> ToArraySegment()
//        {
//            return new ArraySegment<byte>(_buffer, 0, _length);
//        }

//        protected void Reset()
//        {
//            _position = 0;
//            _length = 0;
//        }
//        protected void SetLength(int newLength)
//        {
//            int oldLength = _length;

//            // ensure length & capacity
//            EnsureLength(newLength);

//            // zero out new length
//            if (oldLength < newLength)
//            {
//                Array.Clear(_buffer, oldLength, newLength - oldLength);
//            }

//            _length = newLength;
//            _position = Math.Min(_position, _length);
//        }

//        [MethodImpl(MethodImplOptions.AggressiveInlining)]
//        protected void EnsureLength(int value)
//        {
//            if (_length < value)
//            {
//                _length = value;
//                EnsureCapacity(value);
//            }
//        }

//        [MethodImpl(MethodImplOptions.AggressiveInlining)]
//        protected void EnsureCapacity(int value)
//        {
//            if (_buffer.Length < value)
//            {
//                int capacity = Math.Max(value, _buffer.Length * 2);
//                Array.Resize(ref _buffer, capacity);
//            }
//        }

//        public void Dispose()
//        {
//            _buffer = null;
//            _position = default;
//            _length = default;
//            _encoding = null;
//            _stringBuffer = null;
//        }

//        private byte[] _buffer = new byte[0];
//        private int _position;
//        private int _length;
//        private UTF8Encoding _encoding = new UTF8Encoding(false, true);
//        private byte[] _stringBuffer = new byte[1024 * 32];
//    }
//}

