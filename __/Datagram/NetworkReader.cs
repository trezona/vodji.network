﻿//using System;
//using System.Text;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Interfaces;
//using Vodji.Network.Utils;

//namespace Vodji.Network.Data
//{
//    public class NetworkReader : INetworkReader, IDisposable
//    {
//        public NetworkReader(byte[] bytes) => _buffer = new ArraySegment<byte>(bytes);
//        public NetworkReader(ArraySegment<byte> segment) => _buffer = segment;

//        public byte[] Read() => ReadBytesSegment(Length - Position).Array;

//        public T Read<T>() => Read(typeof(T));
//        public dynamic Read(Type type)
//        {
//            if (type.Equals(typeof(byte)))                  return ReadByte();
//            if (type.Equals(typeof(byte[])))                return ReadBytesAndSize();
//            if (type.Equals(typeof(short)))                 return ReadInt16();
//            if (type.Equals(typeof(int)))                   return ReadInt32();
//            if (type.Equals(typeof(long)))                  return ReadInt64();
//            if (type.Equals(typeof(ushort)))                return ReadUInt16();
//            if (type.Equals(typeof(uint)))                  return ReadUInt32();
//            if (type.Equals(typeof(ulong)))                 return ReadUInt64();
//            if (type.Equals(typeof(sbyte)))                 return ReadSByte();
//            if (type.Equals(typeof(char)))                  return ReadChar();
//            if (type.Equals(typeof(bool)))                  return ReadBoolean();
//            if (type.Equals(typeof(float)))                 return ReadSingle();
//            if (type.Equals(typeof(double)))                return ReadDouble();
//            if (type.Equals(typeof(decimal)))               return ReadDecimal();
//            if (type.Equals(typeof(string)))                return ReadString();
//            if (type.Equals(typeof(Guid)))                  return ReadGuid();
//            if (type.Equals(typeof(Uri)))                   return ReadUri();
//            if (type.Equals(typeof(ArraySegment<byte>)))    return ReadBytesAndSizeSegment();
//            return null;
//        }

//        #region Readers
//        protected virtual byte ReadByte()
//        {
//            if (Position + 1 > _buffer.Count)
//            {
//                LoggingManager.GetLogger().LogError("ReadByte out of range:" + ToString());
//                return 0;
//            }
//            return _buffer.Array[_buffer.Offset + Position++];
//        }
//        protected virtual int ReadInt32() => (int)ReadUInt32();
//        protected virtual long ReadInt64() => (long)ReadUInt64();
//        protected virtual uint ReadUInt32()
//        {
//            uint value = 0;
//            value |= ReadByte();
//            value |= (uint)(ReadByte() << 8);
//            value |= (uint)(ReadByte() << 16);
//            value |= (uint)(ReadByte() << 24);
//            return value;
//        }
//        protected virtual ulong ReadUInt64()
//        {
//            ulong value = 0;
//            value |= ReadByte();
//            value |= ((ulong)ReadByte()) << 8;
//            value |= ((ulong)ReadByte()) << 16;
//            value |= ((ulong)ReadByte()) << 24;
//            value |= ((ulong)ReadByte()) << 32;
//            value |= ((ulong)ReadByte()) << 40;
//            value |= ((ulong)ReadByte()) << 48;
//            value |= ((ulong)ReadByte()) << 56;
//            return value;
//        }
//        protected virtual byte[] ReadBytes(byte[] bytes, int count)
//        {
//            // check if passed byte array is big enough
//            if (count > bytes.Length)
//            {
//                LoggingManager.GetLogger().LogError("ReadBytes can't read " + count + " + bytes because the passed byte[] only has length " + bytes.Length);
//                return null;
//            }

//            ArraySegment<byte> data = ReadBytesSegment(count);
//            Array.Copy(data.Array, data.Offset, bytes, 0, count);
//            return bytes;
//        }
//        protected virtual ArraySegment<byte> ReadBytesSegment(int count)
//        {
//            // check if within buffer limits
//            if (Position + count > _buffer.Count)
//            {
//                LoggingManager.GetLogger().LogError("ReadBytesSegment can't read " + count + " bytes because it would read past the end of the stream. " + ToString());
//                return null;
//            }

//            // return the segment
//            ArraySegment<byte> result = new ArraySegment<byte>(_buffer.Array, _buffer.Offset + Position, count);
//            Position += count;
//            return result;
//        }
//        protected virtual sbyte ReadSByte() => (sbyte)ReadByte();
//        protected virtual char ReadChar() => (char)ReadUInt16();
//        protected virtual bool ReadBoolean() => ReadByte() != 0;
//        protected virtual short ReadInt16() => (short)ReadUInt16();
//        protected virtual ushort ReadUInt16()
//        {
//            ushort value = 0;
//            value |= ReadByte();
//            value |= (ushort)(ReadByte() << 8);
//            return value;
//        }
//        protected virtual float ReadSingle()
//        {
//            UIntFloat converter = new UIntFloat();
//            converter.intValue = ReadUInt32();
//            return converter.floatValue;
//        }
//        protected virtual double ReadDouble()
//        {
//            UIntDouble converter = new UIntDouble();
//            converter.longValue = ReadUInt64();
//            return converter.doubleValue;
//        }
//        protected virtual decimal ReadDecimal()
//        {
//            UIntDecimal converter = new UIntDecimal();
//            converter.longValue1 = ReadUInt64();
//            converter.longValue2 = ReadUInt64();
//            return converter.decimalValue;
//        }
//        protected virtual string ReadString()
//        {
//            // read number of bytes
//            ushort size = ReadUInt16();

//            if (size == 0)
//                return null;

//            int realSize = size - 1;

//            // make sure it's within limits to avoid allocation attacks etc.
//            if (realSize >= 1024 * 30)
//            {
//                LoggingManager.GetLogger().LogError("ReadString too long: " + realSize + ". Limit is: " + 1024 * 30);
//                return null;
//            }

//            ArraySegment<byte> data = ReadBytesSegment(realSize);

//            // convert directly from buffer to string via encoding
//            return _encoding.GetString(data.Array, data.Offset, data.Count);
//        }
//        protected virtual byte[] ReadBytesAndSize()
//        {
//            // count = 0 means the array was null
//            // otherwise count -1 is the length of the array
//            uint count = ReadPackedUInt32();
//            return count == 0 ? null : ReadBytes(checked((int)(count - 1u)));
//        }
//        protected virtual ArraySegment<byte> ReadBytesAndSizeSegment()
//        {
//            // count = 0 means the array was null
//            // otherwise count - 1 is the length of the array
//            uint count = ReadPackedUInt32();
//            return count == 0 ? default : ReadBytesSegment(checked((int)(count - 1u)));
//        }
//        protected virtual int ReadPackedInt32()
//        {
//            // zigzag decoding https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
//            uint data = ReadPackedUInt32();
//            return (int)((data >> 1) ^ -(data & 1));
//        }
//        protected virtual uint ReadPackedUInt32()
//        {
//            // http://sqlite.org/src4/doc/trunk/www/varint.wiki
//            // NOTE: big endian.
//            // Use checked() to force it to throw OverflowException if data is invalid

//            return checked((uint)ReadPackedUInt64());
//        }
//        protected virtual long ReadPackedInt64()
//        {
//            // zigzag decoding https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
//            ulong data = ReadPackedUInt64();
//            return ((long)(data >> 1)) ^ -((long)data & 1);
//        }
//        protected virtual ulong ReadPackedUInt64()
//        {
//            byte a0 = ReadByte();
//            if (a0 < 241)
//            {
//                return a0;
//            }

//            byte a1 = ReadByte();
//            if (a0 >= 241 && a0 <= 248)
//            {
//                return 240 + ((a0 - (ulong)241) << 8) + a1;
//            }

//            byte a2 = ReadByte();
//            if (a0 == 249)
//            {
//                return 2288 + ((ulong)a1 << 8) + a2;
//            }

//            byte a3 = ReadByte();
//            if (a0 == 250)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16);
//            }

//            byte a4 = ReadByte();
//            if (a0 == 251)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24);
//            }

//            byte a5 = ReadByte();
//            if (a0 == 252)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32);
//            }

//            byte a6 = ReadByte();
//            if (a0 == 253)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40);
//            }

//            byte a7 = ReadByte();
//            if (a0 == 254)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48);
//            }

//            byte a8 = ReadByte();
//            if (a0 == 255)
//            {
//                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48) + (((ulong)a8) << 56);
//            }

//            throw new IndexOutOfRangeException("ReadPackedUInt64() failure: " + a0);
//        }
//        protected virtual byte[] ReadBytes(int count)
//        {
//            byte[] bytes = new byte[count];
//            ReadBytes(bytes, count);
//            return bytes;
//        }
//        protected virtual Guid ReadGuid() => new Guid(ReadBytes(16));
//        protected virtual Uri ReadUri() => new Uri(ReadString());
//        #endregion

//        public override string ToString()
//        {
//            return "NetworkReader pos=" + Position + " len=" + Length + " buffer=" + BitConverter.ToString(_buffer.Array, _buffer.Offset, _buffer.Count);
//        }

//        public void Dispose()
//        {
//            _buffer = null;
//            _encoding = null;
//        }

//        public void Reset() => Position = 0;

//        public int Position { get; set; }
//        public int Length => _buffer.Count;

//        private ArraySegment<byte> _buffer;
//        private UTF8Encoding _encoding = new UTF8Encoding(false, true);
//    }

//}
